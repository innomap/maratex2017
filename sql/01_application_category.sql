-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2017 at 04:48 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maratex2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `application_category`
--

CREATE TABLE IF NOT EXISTS `application_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `application_category`
--

INSERT INTO `application_category` (`id`, `name`, `desc`, `parent_id`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'i-MTech (Anugerah Inovasi Sosial)', '', NULL, '2017-01-30 22:49:57', '0000-00-00 00:00:00', 0),
(2, 'i-MMa (Anugerah Inovasi Penyampaian Perkhidmatan)', '', NULL, '2017-01-30 22:49:58', '0000-00-00 00:00:00', 0),
(3, 'i-MEd (Anugerah Inovasi Pembangunan Bahan)', '', NULL, '2017-01-30 22:49:58', '0000-00-00 00:00:00', 0),
(4, 'PERINGKAT Warga Kerja MARA & Subsidiari MARA', '', 1, '2017-02-01 07:02:04', '0000-00-00 00:00:00', 0),
(5, 'PERINGKAT PELAJAR', '', 1, '2017-02-01 07:02:04', '0000-00-00 00:00:00', 0),
(6, 'Pengurusan Organisasi', '', 2, '2017-02-01 07:03:42', '0000-00-00 00:00:00', 0),
(7, 'Pengurusan IPMA/ Sekolah', '', 2, '2017-02-01 07:03:42', '0000-00-00 00:00:00', 0),
(8, 'Bahan Latihan &  Kompetensi (BLK) ', '', 3, '2017-02-01 07:05:22', '0000-00-00 00:00:00', 0),
(9, 'Bahan Bantu Mengajar (BBM) ', '', 3, '2017-02-01 07:05:22', '0000-00-00 00:00:00', 0),
(10, 'Bahan Bantu Belajar (BBB)', '', 3, '2017-02-01 07:05:51', '0000-00-00 00:00:00', 0),
(11, 'Generik', '(Contoh: Communication, Problem-Solving, Planning & Managing, Customer Service, Policy & Procedures)', 8, '2017-02-01 07:09:51', '0000-00-00 00:00:00', 0),
(12, 'Functional', '(Contoh: Strategic Planning, Procurement, Auditing, Marketing, Human Resource Management, Knowledge Management)', 8, '2017-02-01 07:09:51', '0000-00-00 00:00:00', 0),
(13, 'PENGAJARAN  BAHASA INGGERIS', '', 9, '2017-02-01 07:11:06', '0000-00-00 00:00:00', 0),
(14, 'PENGAJARAN SAINS', '', 9, '2017-02-01 07:11:06', '0000-00-00 00:00:00', 0),
(15, 'PENGAJARAN MATEMATIK', '', 9, '2017-02-01 07:11:06', '0000-00-00 00:00:00', 0),
(16, 'PEMBELAJARAN BAHASA INGGERIS', '', 10, '2017-02-01 07:12:48', '0000-00-00 00:00:00', 0),
(17, 'PEMBELAJARAN SAINS', '', 10, '2017-02-01 07:12:48', '0000-00-00 00:00:00', 0),
(18, 'PEMBELAJARAN MATEMATIK', '', 10, '2017-02-01 07:12:48', '0000-00-00 00:00:00', 0),
(19, 'SAINS (MENENGAH RENDAH)', '', 14, '2017-02-01 07:15:10', '0000-00-00 00:00:00', 0),
(20, 'BIOLOGI ', '', 14, '2017-02-01 07:15:10', '0000-00-00 00:00:00', 0),
(21, 'KIMIA', '', 14, '2017-02-01 07:15:10', '0000-00-00 00:00:00', 0),
(22, 'FIZIK', '', 14, '2017-02-01 07:15:10', '0000-00-00 00:00:00', 0),
(23, 'SAINS (MENENGAH RENDAH)', '', 17, '2017-02-01 07:15:55', '0000-00-00 00:00:00', 0),
(24, 'BIOLOGI ', '', 17, '2017-02-01 07:15:55', '0000-00-00 00:00:00', 0),
(25, 'KIMIA', '', 17, '2017-02-01 07:15:55', '0000-00-00 00:00:00', 0),
(26, 'FIZIK', '', 17, '2017-02-01 07:15:55', '0000-00-00 00:00:00', 0),
(27, 'MATEMATIK (MENENGAH RENDAH) ', '', 15, '2017-02-01 07:17:02', '0000-00-00 00:00:00', 0),
(28, 'MATEMATIK (MENENGAH ATAS) ', '', 15, '2017-02-01 07:17:02', '0000-00-00 00:00:00', 0),
(29, 'MATEMATIK TAMBAHAN ', '', 15, '2017-02-01 07:17:02', '0000-00-00 00:00:00', 0),
(30, 'MATEMATIK (MENENGAH RENDAH) ', '', 18, '2017-02-01 07:17:41', '0000-00-00 00:00:00', 0),
(31, 'MATEMATIK (MENENGAH ATAS) ', '', 18, '2017-02-01 07:17:41', '0000-00-00 00:00:00', 0),
(32, 'MATEMATIK TAMBAHAN ', '', 18, '2017-02-01 07:17:41', '0000-00-00 00:00:00', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `application_category`
--
ALTER TABLE `application_category`
  ADD CONSTRAINT `application_category_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `application_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
