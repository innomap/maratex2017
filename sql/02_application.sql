-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 07, 2017 at 04:48 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maratex2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_category_id` int(11) DEFAULT NULL,
  `project_title` varchar(255) NOT NULL,
  `project_description` mediumtext NOT NULL,
  `potential` mediumtext NOT NULL,
  `significant` mediumtext NOT NULL,
  `relevancy` mediumtext NOT NULL,
  `outcome` mediumtext NOT NULL,
  `sustainability` mediumtext NOT NULL,
  `readiness_opt` tinyint(1) DEFAULT NULL,
  `readiness` mediumtext NOT NULL,
  `advisor_name` varchar(255) NOT NULL,
  `advisor_photo` varchar(255) NOT NULL,
  `advisor_kp_no` varchar(100) NOT NULL,
  `advisor_email` varchar(255) NOT NULL,
  `advisor_telp_no` varchar(30) NOT NULL,
  `advisor_fax_no` varchar(30) NOT NULL,
  `headquarters_approval` tinyint(1) NOT NULL,
  `headquarters_approval_attachment` varchar(255) NOT NULL,
  `participant_approval` tinyint(1) NOT NULL,
  `participant_approval_attachment` varchar(255) NOT NULL,
  `round_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `submission_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`application_category_id`),
  KEY `application_category_id` (`application_category_id`),
  KEY `round_id` (`round_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `application`
--
ALTER TABLE `application`
  ADD CONSTRAINT `application_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `application_ibfk_2` FOREIGN KEY (`application_category_id`) REFERENCES `application_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `application_ibfk_3` FOREIGN KEY (`round_id`) REFERENCES `round` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
