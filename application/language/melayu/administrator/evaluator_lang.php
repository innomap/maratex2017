<?php
	$lang['new_evaluator'] = "Penilai Baru";
	$lang['name'] = "Nama";
	$lang['telp_no'] = "No Telefon";
	$lang['address'] = "Alamat";
	$lang['email'] = "Emel";
	$lang['status'] = "Status";
	$lang['created_at'] = "Masa Dicipta";
	$lang['action'] = "Aksi";
	$lang['save'] = "Simpan";
	$lang['cancel'] = "Batal";
	$lang['edit'] = "Ubah";
	$lang['delete'] = "Padam";
	$lang['delete_evaluator'] = "Padam Penilai";
	$lang['delete_confirm_message'] = "Anda yakin akan memadam ";
	$lang['form_evaluator'] = "Borang Penilai";
	$lang['active'] = "Aktif";
	$lang['suspended'] = "Suspend";
	$lang['change_password'] = "Ubah Kata Laluan";
	$lang['password'] = "Kata Laluan";
	$lang['retype_password'] = "Ulang Kata Laluan";
?>