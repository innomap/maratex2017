<?php
	$lang['new_article'] = "Artikel Baru";
	$lang['menu_name'] = "Nama Menu";
	$lang['title'] = "Tajuk";
	$lang['content'] = "Kandungan";
	$lang['status'] = "Status";
	$lang['created_at'] = "Masa Dicipta";
	$lang['action'] = "Aksi";
	$lang['save'] = "Simpan";
	$lang['cancel'] = "Batal";
	$lang['edit'] = "Ubah";
	$lang['delete'] = "Padam";
	$lang['delete_article'] = "Padam Artikel";
	$lang['delete_confirm_message'] = "Anda yakin akan memadam ";
	$lang['form_article'] = "Borang Artikel";
	$lang['published'] = "Diterbitkan";
	$lang['draft'] = "Draf";
?>