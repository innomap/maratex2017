<?php
	$lang['email'] = "Emel";
	$lang['password'] = "Kata Laluan";
	$lang['forgot_password'] = "Lupa Kata Laluan";
	$lang['dont_have_an_account'] = "Belum membuat pendaftaran";
	$lang['sign_up'] = "Daftar";
	$lang['registration_form'] = "Pendaftaran Sistem";
	$lang['retype_password'] = 'Retype Password';
	$lang['name'] = "Nama";
	$lang['kp_no'] = "No KP";
	$lang['telp_no'] = "No Telefon";
	$lang['fax_no'] = "No Faks";
	$lang['mara_center'] = "Pusat Mara";
	$lang['mara_address'] = "Alamat Pusat";
	$lang['mara_telp_no'] = "No Telefon Pusat";
	$lang['mara_fax_no'] = "No Faks Pusat";
	$lang['innovator_photo'] = "Foto Ketua Projek";
	$lang['already_have_account'] = "Sudah mempunyai akaun";
?>