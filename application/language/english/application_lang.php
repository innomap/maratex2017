<?php 
	$lang['new_application'] = "New Application";
	$lang['project_title'] = "Project Title";
	$lang['description'] = "Description";
	$lang['status'] = "Status";
	$lang['action'] = "Action";
	$lang['mara_center'] = "Mara Center";
	$lang['application_form'] = "Application Form";
	$lang['mara_address'] = "Mara Address";
	$lang['mara_telp_no'] = "Mara Telp No";
	$lang['mara_fax_no'] = "Mara Fax No";
	$lang['project_title'] = "Project Title";
	$lang['project_description'] = "Project Description";
	$lang['tips_project_description'] = "Creative and innovative, new ideas and original / purity (Originality), in accordance with the objectives of the center / top of MARA, uniqueness, adaptation (improvement / renovation of existing ideas)";
	$lang['potential'] = "Project Potential";
	$lang['tips_potential'] = "Applicable and costs can be controlled, can Expanded Use, Improvement, Customer Satisfaction";
	$lang['significant'] = "Significant";
	$lang['tips_significant'] = "Stakeholders impact, the impact on the individual, the center / top of MARA, organizations, communities, states, and other global, effectiveness (Effectiveness) Problem Solver (Problem-solving) Value for Money (savings in terms of cost or worth it)";
	$lang['relevancy'] = "Relevancy";
	$lang['tips_relevancy'] = "Practical, Usability, Innovation Works (Functional), easy to use, easily maintained (Easy maintenance) efficient in terms of cost / time, a significant increase in quality and productivity";
	$lang['outcome'] = "Outcome";
	$lang['tips_outcome'] = "Savings, Increased Productivity, Value, Time Process";
	$lang['sustainability'] = "Sustainability";
	$lang['tips_sustainability'] = "Can be accepted by other organization, the latest techniques, innovations give continuity to the durability of facing technological and economic changes, environmental friendly";
	$lang['product_pictures'] = "Product Pictures";
	$lang['application_category'] = "Application Category";
	$lang['innovator'] = 'Innovator';
	$lang['kp_no'] = "KP No";
	$lang['telp_no'] = "Telephone No";
	$lang['fax_no'] = "Fax No";
	$lang['innovator_photo'] = "Innovator Photo";
	$lang['email'] = "Email";
	$lang['team_member'] = "Team Member";
	$lang['name'] = "Name";
	$lang['advisor_photo'] = "Advisor Photo";
	$lang['advisor'] = "Advisor";
	$lang['tips_advisor'] = "Name as in Identity Card";
	$lang['headquarters_approval'] = "Headquarters Approval";
	$lang['view_attachment'] = "View Attachment";
	$lang['has_headquarters_approval'] = "Has headquarters approval";
	$lang['participant_approval'] = "Participant Approval";
	$lang['has_participant_approval'] = "Has participant approval";
	$lang['cancel'] = "Cancel";
	$lang['back'] = "Back";
	$lang['save'] = "Save";
	$lang['send_for_approval'] = "Send for Approval";
	$lang['photo'] = "Photo";
	$lang['readiness'] = "Readiness";
	$lang['send_for_approval_confirm_message'] = "Once you submit you can not edit this application. Are you sure want to submit this application?";
	$lang['send_application_for_approval'] = "Send Application for Approval";
	$lang['view_uploaded_attachment'] = "View Uploaded Attachment";
	$lang['delete'] = "Delete";
	$lang['delete_application'] = "Delete Application";
	$lang['delete_confirm_message'] = "Are you sure want to delete ";
?>