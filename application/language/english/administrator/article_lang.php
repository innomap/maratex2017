<?php
	$lang['new_article'] = "New Article";
	$lang['menu_name'] = "Menu Name";
	$lang['title'] = "Title";
	$lang['content'] = "Content";
	$lang['status'] = "Status";
	$lang['created_at'] = "Created At";
	$lang['action'] = "Action";
	$lang['save'] = "Save";
	$lang['cancel'] = "Cancel";
	$lang['edit'] = "Edit";
	$lang['delete'] = "Delete";
	$lang['delete_article'] = "Delete Article";
	$lang['delete_confirm_message'] = "Are you sure want to delete ";
	$lang['form_article'] = "Article Form";
	$lang['published'] = "Published";
	$lang['draft'] = "Draft";
?>