<?php
	$lang['new_evaluator'] = "New Evaluator";
	$lang['name'] = "Menu Name";
	$lang['telp_no'] = "Telp No";
	$lang['address'] = "Address";
	$lang['email'] = "Email";
	$lang['status'] = "Status";
	$lang['created_at'] = "Created At";
	$lang['action'] = "Action";
	$lang['save'] = "Save";
	$lang['cancel'] = "Cancel";
	$lang['edit'] = "Edit";
	$lang['delete'] = "Delete";
	$lang['delete_evaluator'] = "Delete Evaluator";
	$lang['delete_confirm_message'] = "Are you sure want to delete ";
	$lang['form_evaluator'] = "Evaluator Form";
	$lang['active'] = "Active";
	$lang['suspended'] = "Suspended";
	$lang['change_password'] = "Change Password";
	$lang['password'] = "Password";
	$lang['retype_password'] = "Confirm Password";
?>