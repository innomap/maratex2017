<?php
	$lang['email'] = "Email";
	$lang['status'] = "Status";
	$lang['created_at'] = "Created At";
	$lang['action'] = "Action";
	$lang['active'] = "Active";
	$lang['suspended'] = "Suspended";
	$lang['save'] = "Save";
	$lang['cancel'] = "Cancel";
	$lang['change_password'] = "Change Password";
	$lang['password'] = "Password";
	$lang['retype_password'] = "Confirm Password";
?>