<?php
	$lang['email'] = "Email";
	$lang['password'] = "Password";
	$lang['forgot_password'] = "Forgot Password";
	$lang['dont_have_an_account'] = "Don't have an account";
	$lang['sign_up'] = "Sign Up";
	$lang['registration_form'] = "Registration Form";
	$lang['retype_password'] = 'Retype Password';
	$lang['name'] = 'Name';
	$lang['kp_no'] = "KP No";
	$lang['telp_no'] = "Telephone No";
	$lang['fax_no'] = "Fax No";
	$lang['mara_center'] = 'Mara Center';
	$lang['mara_address'] = "Mara Address";
	$lang['mara_telp_no'] = "Mara Telp No";
	$lang['mara_fax_no'] = "Mara Fax No";
	$lang['innovator_photo'] = "Innovator Photo";
	$lang['already_have_account'] = "Already have account";
?>