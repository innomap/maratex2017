<div class="col-md-12">
    <div class="alert alert-info alert-dismissable hide">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>
	<table id="table-application" class="table table-bordered table-striped table-dark">
	    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('name') ?></th>
				<th><?= lang('email') ?></th>
				<th><?= lang('status') ?></th>
				<th><?= lang('kp_no') ?></th>
				<th><?= lang('mara_center') ?></th>
				<th><?= lang('created_at') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
	    </thead>
	    <tbody>
	    	<?php $no = 1; foreach ($applications as $application) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $application['name'] ?></td>
				    <td><?= $application['email'] ?></td>
				    <td><?= ($application['status'] ? "<span class='text-success'>" . lang('active') . "</span>" : "<span class='text-danger'>" . lang('suspended') . "</span>") ?></td>
				    <td><?= $application['kp_no'] ?></td>
				    <td><?= $application['mara_center'] ?></td>
				    <td><?= $application['created_at'] ?></td>
				    <td>
				    	<a href="<?=base_url() . PATH_TO_ADMIN . "applications/view/" . $application['id']?>" class="btn btn-default btn-action btn-edit-group" title="<?= lang('view') ?>">
						    <span class="fa fa-search fa-lg"></span>
						</button>
						<!-- <button type="button" class="btn btn-default btn-action btn-edit-group" title="<?= lang('edit') ?>" data-id="<?= $value['id'] ?>">
						    <span class="fa fa-pencil fa-lg"></span>
						</button>
						<button type="button" class="btn btn-default btn-action btn-delete-group text-red" title="<?= lang('delete') ?>" data-id="<?= $value['id'] ?>"
							data-name="<?= $value['name'] ?>">
						    <span class="fa fa-trash-o fa-lg"></span>
						</button> -->
				    </td>
				</tr>
		    <?php $no++; } ?>
	    </tbody>
	</table>
</div>