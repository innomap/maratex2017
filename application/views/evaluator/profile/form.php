<div class="col-md-12">
    <div class="col-md-12 text-center">
		<h2><?= lang('form_evaluator') ?></h2>
    </div>
    <div class="col-md-8 col-md-offset-2 content-box">
		<form role="form" class="form-horizontal" id="form-evaluator" action="<?= site_url(PATH_TO_EVALUATOR.'profile/save') ?>" method="POST">
			<input type="hidden" name="user_id" value="<?=$this->userdata['id']?>" />
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('name') ?></label>
				<div class="col-md-9">
					<input type="text" name="name" class="form-control" value="<?= isset($detail) ? $detail['name'] : '' ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('address') ?></label>
				<div class="col-md-9">
					<textarea name="address" class="form-control"><?= isset($detail) ? $detail['address'] : '' ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
				<div class="col-md-9">
					<input type="tel" name="telp_no" class="form-control" value="<?= isset($detail) ? $detail['telp_no'] : '' ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('email') ?></label>
				<div class="col-md-9">
					<input type="email" name="email" class="form-control" value="<?= isset($detail) ? $detail['email'] : '' ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('password') ?></label>
				<div class="col-md-9">
					<button type="button" data-toggle="collapse" data-target="#password-form" class="btn btn-info"><?= lang('change_password') ?></button>
				</div>
			</div>	

			<div id="password-form" class="collapse">
				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('password') ?></label>
					<div class="col-md-9">
						<input type="password" name="password" class="form-control" placeholder="<?= lang('password') ?>" />
					</div>
				</div>	

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('retype_password') ?></label>
					<div class="col-md-9">
						<input type="password" name="retype_password" class="form-control" placeholder="<?= lang('retype_password') ?>" />
					</div>
				</div>	
			</div>		
			
			<div class="form-group text-center">
				<button type="button" class="btn btn-default flat" onclick="window.history.back()"><?= lang('cancel') ?></button>
				<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
			</div>
		</form>
    </div>
</div>