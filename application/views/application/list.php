<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_application = "<?= lang('delete_application') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		lang_application_submitted_msg = "<?= lang('application_submitted_msg') ?>";
</script>
<div class="col-md-12">
	<div class="col-md-8 col-md-offset-2">
		<?php $this->load->view('partial/logo_after_login',array('title' => '')); ?>
	</div>	
    <div class="col-md-12 margin-btm-10">
		<a href="<?= base_url().'applications/add' ?>">
			<button type="button" class="btn btn-success">
				<span class="fa fa-plus"></span> <?= lang('new_application') ?>
			</button>
		</a>
    </div>

    <div class="col-md-12 table-responsive content-box">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<table id="table-application" class="table table-bordered table-striped table-dark">
		    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('project_title') ?></th>
				<th><?= lang('description') ?></th>
				<th><?= lang('status') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php $no = 1; foreach ($applications as $key=>$value) { ?>
			<tr>
			    <td><?= $no ?></td>
			    <td><?= $value->project_title ?></td>
			    <td><?= ellipsis($value->project_description, 100) ?></td>
			    <td class="<?= $status[$value->status]['class'] ?>"><?= $status[$value->status]['name'] ?></td>
			    <td>
			    	<?php if($value->status == APPLICATION_STATUS_DRAFT){ ?>
						<button type="button" class="btn btn-default btn-action btn-edit-application" title="<?= lang('edit') ?>" data-id="<?= $value->id ?>">
						    <span class="fa fa-pencil fa-lg"></span>
						</button>

						<button type="button" class="btn btn-default btn-action btn-delete-application text-red" data-id="<?= $value->id ?>"
						data-name="<?= $value->project_title ?>" title="<?= lang('delete') ?>">
						    <span class="fa fa-trash-o fa-lg"></span>
						</button>
					<?php }else{ ?>
						<button type="button" class="btn btn-default btn-action btn-view-application" title="<?= lang('view_detail') ?>" data-id="<?= $value->id ?>">
						    <span class="fa fa-search fa-lg"></span> 
						</button>
					<?php } ?>
			    </td>
			</tr>
		    <?php $no++;} ?>
		    </tbody>
		</table>
    </div>
</div>