<script type="text/javascript">
	var alert = "<?= $alert ?>",
		alert_dialog = "<?= $alert_dialog ?>";
	var	lang_send_for_approval_confirm_message = "<?= lang('send_for_approval_confirm_message') ?>",
        lang_send_application_for_approval = "<?= lang('send_application_for_approval') ?>",
        lang_cancel = "<?= lang('cancel') ?>",
        lang_send_for_approval = "<?= lang('send_for_approval') ?>",
        lang_yes = "<?= lang('yes') ?>",
        lang_application_submitted_msg = "<?= lang('application_submitted_msg') ?>";

	var view_mode = "<?= ($view_mode == 1 ? true : false) ?>";
</script>
<div class="col-md-8 col-md-offset-2 margin-content bg-grey">
	<?php $this->load->view('partial/logo_after_login',array('title' => lang('application_form'))); ?>

	<div class="col-md-12">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
	</div>

	<form id="form-application" class="form-horizontal" action="<?= base_url().'applications/'.$action_url ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= (isset($app) ? $app->id : 0) ?>">

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_center') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $person->mara_center->name ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_address') ?></label>
			<div class="col-md-9">
				<textarea class="form-control" disabled><?= $person->mara_address ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $person->mara_telp_no ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_fax_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $person->mara_fax_no ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('application_category') ?></label>
			<div class="col-md-9">
				<?= $category_content ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('project_title') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="project_title" placeholder="<?= lang('project_title') ?>" value="<?= (isset($app) ? $app->project_title : "") ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('project_description') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_project_description') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="project_description" rows="5" placeholder="<?= lang('tips_project_description') ?>"><?= (isset($app) ? $app->project_description : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('potential') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_potential') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="potential" rows="5" placeholder="<?= lang('tips_potential') ?>"><?= (isset($app) ? $app->potential : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('significant') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_significant') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="significant" rows="5" placeholder="<?= lang('tips_significant') ?>"><?= (isset($app) ? $app->significant : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('relevancy') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_relevancy') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="relevancy" rows="5" placeholder="<?= lang('tips_relevancy') ?>"><?= (isset($app) ? $app->relevancy : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('outcome') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_outcome') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="outcome" rows="5" placeholder="<?= lang('tips_outcome') ?>"><?= (isset($app) ? $app->outcome : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('readiness') ?></label>
			<div class="col-md-9">
				<?= $project_readiness_content; ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-9 col-md-offset-3">
				<textarea class="form-control" name="readiness" rows="5" placeholder="<?= lang('readiness') ?>"><?= (isset($app) ? $app->readiness : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('sustainability') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_sustainability') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="sustainability" rows="5" placeholder="<?= lang('tips_sustainability') ?>"><?= (isset($app) ? $app->sustainability : "") ?></textarea>
			</div>
		</div>

		<div class="form-group picture-wrapper">
			<label class="col-md-3 control-label"><?= lang('product_pictures') ?></label>

			<div class="col-md-9 item">
				<?php if(isset($app)){
						foreach ($app->pictures as $key => $value) { 
							if(file_exists(realpath(APPPATH . '../'.PATH_TO_APPLICATION_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value->name)) {
								$url = site_url() . 'assets/attachment/application_picture/thumbnail/' . $value->name;
							}else{
								$url = site_url() . 'assets/attachment/application_picture/thumbnail/default.jpg';
							} ?>
							<div class="col-md-12">
								<a href="<?= $url ?>" class="fancyboxs">
									<img src="<?= $url ?>" width="300px">
								</a>
								<button type="button" class="btn btn-danger delete-picture" data-id="<?= $value->id ?>" data-name="<?= $value->name ?>"><span class="fa fa-times"></span></button>
							</div>
				<?php 	}
					} ?>

				<div class="col-md-11">
					<input type="file" class="form-control picture-item" name="picture_0">
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-grey add-picture"><span class="fa fa-plus"></span></button>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovator_photo') ?></label>
			<div class="col-md-9">
				<?php if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $person->photo) && $person->photo != "") { ?>
						<a href="<?= base_url().PATH_TO_INNOVATOR_PHOTO.$person->photo ?>" class="fancyboxs"><img src="<?= base_url().PATH_TO_INNOVATOR_PHOTO_THUMB.$person->photo ?>" width="30%"/></a>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovator') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" placeholder="<?= lang('innovator') ?>" value="<?= $person->name ?>" readonly>
				<input type="hidden" name="user_id" value="<?= $person->user_id ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('kp_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $person->kp_no ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('email') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $person->user->email ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $person->telp_no ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('fax_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $person->fax_no ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('team_member') ?></label>
			<div class="col-md-9 team-member-wrap">
				<?php if(isset($app)){
						foreach ($app->experts as $key => $value) {
							if($value->photo){
								if(file_exists(realpath(APPPATH . '../'.PATH_TO_TEAM_MEMBER_PHOTO_THUMB) . DIRECTORY_SEPARATOR . $value->photo)) {
									$url = site_url() . PATH_TO_TEAM_MEMBER_PHOTO_THUMB . $value->photo;
								}else{
									$url = "";
								}
							}else{ $url = ""; } ?> 
							<input type="hidden" class="team-member-id" name="team_member_id[]" value="<?= $value->id ?>">
							<div class="col-md-11 team-member-item">
								<input type="hidden" name="h_team_member_pic_<?= $key ?>" value="<?= $value->photo ?>">
								<input type="hidden" name="i_deleted_team_member_<?= $key ?>">
								<div class="col-md-6">
									<input type="text"class="form-control" name="team_member_<?= $key ?>" placeholder="<?= lang('name') ?>" value="<?= $value->name ?>">
								</div>
								<div class="col-md-3">
									<input type="text"class="form-control" name="team_member_kp_<?= $key ?>" placeholder="<?= lang('kp_no') ?>" value="<?= $value->kp_no ?>">
								</div>
								<div class="col-md-3">
									<input type="text"class="form-control" name="team_member_telp_<?= $key ?>" placeholder="<?= lang('telp_no') ?>" value="<?= $value->telp_no ?>">
								</div>
								<div class="col-md-12 margin-tb-10">
									<div class="col-md-1">
										<label><?= lang('photo') ?></label>
									</div>
									<?php if($url != "" ){ ?>
										<div class="col-md-4">
											<a href="<?= $url ?>" class="fancyboxs">
												<img src="<?= $url ?>" width="100%">
											</a>
										</div>
									<?php } ?>
									<div class="col-md-<?= $url != "" ? '7' : '11' ?> no-padding">
										<input type="file" class="form-control" name="team_member_pic_<?= $key ?>">
									</div>
								</div>
							</div>
							<?php if($key == 0){ ?>
								<div class="col-md-1">
									<button type="button" class="btn btn-grey add-team-member"><span class="fa fa-plus"></span></button>
								</div>
				<?php 		}else{ ?>
								<div class="col-md-1">
									<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>
								</div>
				<?php		}
						}
					  }else{ ?>
					  	<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">
						<div class="col-md-11 team-member-item">	
							<input type="hidden" name="h_team_member_pic_0" value="">
							<input type="hidden" name="i_deleted_team_member_0">
							<div class="col-md-6">
								<input type="text"class="form-control" name="team_member_0" placeholder="<?= lang('name') ?>">
							</div>
							<div class="col-md-3">
								<input type="text"class="form-control" name="team_member_kp_0" placeholder="<?= lang('kp_no') ?>">
							</div>
							<div class="col-md-3">
								<input type="text"class="form-control" name="team_member_telp_0" placeholder="<?= lang('telp_no') ?>">
							</div>
							<div class="col-md-12 margin-tb-10">
								<div class="col-md-1">
									<label><?= lang('photo') ?></label>
								</div>
								<div class="col-md-11 no-padding">
									<input type="file" class="form-control" name="team_member_pic_0">
								</div>
							</div>
						</div>
						<div class="col-md-1">
							<button type="button" class="btn btn-grey add-team-member"><span class="fa fa-plus"></span></button>
						</div>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('advisor_photo') ?></label>
			<?php if(isset($app)){
				if(file_exists(realpath(APPPATH . '../'.PATH_TO_ADVISOR_PHOTO_THUMB) . DIRECTORY_SEPARATOR . $app->advisor_photo)) {
					$url = site_url() . PATH_TO_ADVISOR_PHOTO_THUMB . $app->advisor_photo;
				}else{
					$url = "";
				}
			}else{ $url = ""; } ?> 

			<?php if($url != ""){ ?>
				<div class="col-md-4">
					<img src="<?= $url ?>" width="100%">
				</div>
			<?php } ?>
			<div class="col-md-<?= ($url != "" ? '5' : '9') ?>">
				<input type="hidden" name="h_advisor_photo" value="<?= (isset($app) ? $app->advisor_photo : '') ?>">
				<input type="file" class="form-control" name="advisor_photo">
			</div>
		</div>

		<div class="form-group field-instructor">
			<label class="col-md-3 control-label"><?= lang('advisor') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_advisor') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<input type="text" name="advisor" class="form-control" placeholder="<?= lang('advisor') ?>" value="<?= isset($app) ? $app->advisor_name : '' ?>">
			</div>
		</div>

		<div class="form-group field-instructor">
			<label class="col-md-3 control-label"><?= lang('kp_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="advisor_kp_no" class="form-control" placeholder="<?= lang('kp_no') ?>" value="<?= isset($app) ? $app->advisor_kp_no : '' ?>">
			</div>
		</div>

		<div class="form-group field-instructor">
			<label class="col-md-3 control-label"><?= lang('email') ?></label>
			<div class="col-md-9">
				<input type="text" name="advisor_email" class="form-control" placeholder="<?= lang('email') ?>" value="<?= isset($app) ? $app->advisor_email : '' ?>">
			</div>
		</div>

		<div class="form-group field-instructor">
			<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="advisor_telp_no" class="form-control" placeholder="<?= lang('telp_no') ?>" value="<?= isset($app) ? $app->advisor_telp_no : '' ?>">
			</div>
		</div>

		<div class="form-group field-instructor">
			<label class="col-md-3 control-label"><?= lang('fax_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="advisor_fax_no" class="form-control" placeholder="<?= lang('fax_no') ?>" value="<?= isset($app) ? $app->advisor_fax_no : '' ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('headquarters_approval') ?></label>
			<div class="col-md-9">
				<div class="checkbox"> 
					<label> <input type="checkbox" name="headquarters_approval" class="has-validation" value="1" <?= isset($app) ? ($app->headquarters_approval_attachment != "" && $app->headquarters_approval ? 'checked' : '') : '' ?>> <?= lang('has_headquarters_approval') ?> </label> 
				</div>
				<div class="col-md-12 no-padding">
					<div class="col-md-4">
						<a href="<?= base_url().ASSETS_FILE.'borang_pengesahan_aim_2017.docx' ?>" class="btn-download-wrap" target="_blank">
							<?= lang('view_attachment') ?>
						</a>
					</div>
					<?php if(isset($app)){
						if(file_exists(realpath(APPPATH . '../'.PATH_TO_HEAD_APPROVAL_ATTACHMENT) . DIRECTORY_SEPARATOR . $app->headquarters_approval_attachment)) {
							$url = site_url() . PATH_TO_HEAD_APPROVAL_ATTACHMENT . $app->headquarters_approval_attachment;
						}else{
							$url = "";
						}
					}else{ $url = ""; } ?> 

					<?php if($url != ""){ ?>
						<div class="col-md-6">
							<a href="<?= $url ?>" target="_blank">
								<?= lang('view_uploaded_attachment') ?>
							</a>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-9 no-padding head-approval">
					<input type="hidden" name="h_headquarters_approval_attachment" value="<?= (isset($app) ? $app->headquarters_approval_attachment : '') ?>">
					<input type="file" class="form-control" name="headquarters_approval_attachment">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('participant_approval') ?></label>
			<div class="col-md-9">
				<div class="checkbox"> 
					<label> <input type="checkbox" name="participant_approval" class="has-validation" value="1" <?= isset($app) ? ($app->participant_approval_attachment != "" && $app->participant_approval == 1 ? 'checked' : '') : '' ?>> <?= lang('has_participant_approval') ?> </label> 
				</div>
				<div class="col-md-12 no-padding">
					<div class="col-md-4">
						<a href="<?= base_url().ASSETS_FILE.'borang_perakuan_peserta.doc' ?>" class="btn-download-wrap" target="_blank">
							<?= lang('view_attachment') ?>
						</a>
					</div>
					
					<?php if(isset($app)){
						if(file_exists(realpath(APPPATH . '../'.PATH_TO_PARTICIPANT_APPROVAL_ATTACHMENT) . DIRECTORY_SEPARATOR . $app->participant_approval_attachment)) {
							$url = site_url() . PATH_TO_PARTICIPANT_APPROVAL_ATTACHMENT . $app->participant_approval_attachment;
						}else{
							$url = "";
						}
					}else{ $url = ""; } ?> 

					<?php if($url != ""){ ?>
						<div class="col-md-6">
							<a href="<?= $url ?>" target="_blank">
								<?= lang('view_uploaded_attachment') ?>
							</a>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-9 no-padding participant-approval">
					<input type="hidden" name="h_participant_approval_attachment" value="<?= (isset($app) ? $app->participant_approval_attachment : '') ?>">
					<input type="file" class="form-control" name="participant_approval_attachment">
				</div>
			</div>
		</div>

		<div class="col-md-12 text-center">
			<button type="button" class="btn btn-default btn-back flat" onclick="window.history.back()"><?= ($this->userdata['role_id'] == ROLE_INNOVATOR ? lang('cancel') : lang('back')) ?></button>
			<input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-success flat" value="<?= lang('save') ?>">
			<input type="submit" name="btn_send_for_approval" data-id="btn_send_for_approval" class="btn btn-success flat btn-send-for-approval" data-id="<?= isset($innovation) ? $innovation['innovation_id'] : 0 ?>" value="<?= lang('send_for_approval') ?>">
		</div>
	</form>
</div>

<div class="modal" id="please_wait_dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="padding:0 15px;">
                <h4>Processing...</h4>
            </div>
            <div class="modal-body text-center">
                <img src="<?= base_url() . ASSETS_IMG . 'loading.gif' ?>">
            </div>
        </div>
    </div>
</div>