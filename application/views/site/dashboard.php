<div class="container-fluid landing-page no-padding">
	<div class="col-md-offset-1 col-md-11 no-padding" style="margin-top: 10%;">
		<img src="<?= base_url() ?>/assets/img/new-logo-maratex.png" class="img-responsive">
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 no-padding footer">
        <div class="foot-menu">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-footer" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar-footer" class="navbar-collapse collapse" style="height: 1px;">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">PENGENALAN</a></li>
                        <li class=""><a href="#">BERITA</a></li>
                        <li class=""><a href="#">PERTANDINGAN</a></li>
                        <li class=""><a href="<?= base_url().'login' ?>">BORANG PENYERTAAN</a></li>
                        <li class=""><a href="#">SOALAN LAZIM</a></li>
                        <li class=""><a href="#">SEKRETARIAT</a></li>
                        
                    </ul>
                </div>
            </nav>
            <div class="col-md-8" style="margin: 0 228px;padding: 0 5px; box-shadow: #000 0px 15px 30px -15px;">
	        	<div class="border"></div>
		        <div class="border-silver">
		        	<div class="col-md-4" style="border-right: 1px solid #e2e2e2;padding: 10px 20px;">
						<img src="<?= base_url() ?>/assets/img/logo_maratex.png" style="margin:0 auto; width: 100%;">
		        	</div>
		        	<div class="col-md-3" style="border-right: 1px solid #e2e2e2;padding: 10px 20px;">
						<img src="<?= base_url() ?>/assets/img/logo_mtech.png" style="margin:0 auto;width: 180px;">
		        	</div>
		        	<div class="col-md-3" style="border-right: 1px solid #e2e2e2;padding: 10px 20px;">
						<img src="<?= base_url() ?>/assets/img/logo_imma.png" style="margin:0 auto;width: 180px;">
		        	</div>
		        	<div class="col-md-2" style="border-right: 1px solid #e2e2e2;padding: 10px 20px;">
						<img src="<?= base_url() ?>/assets/img/logo_imed.png" style="width: 100%;">
		        	</div>
		        </div>
	        </div>
        </div>
        <div class="foot-content col-md-12 text-center">
            <img src="http://maratex2016.innomap.my/assets/img/logo-mara.png" width="27px">
            <span>Organised by MARA Innovation and Research Unit in collaboration with Yayasan Inovasi Malaysia</span>
            <img src="http://maratex2016.innomap.my/assets/img/logo-mini.png" width="30px">
        </div>
    </div>
</div>