<div class="col-md-12" style="margin-bottom:100px">
	<div class="col-md-12 text-center">
		<h2><?= lang('form_group') ?></h2>
	</div>
	<div class="col-md-8 col-md-offset-2 content-box">
		<form role="form" class="form-horizontal" id="form-profile" action="<?= site_url('profile/save') ?>" method="POST" enctype="multipart/form-data">
			<h3>Innovator Detail</h3>
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('name') ?></label>
				<div class="col-md-9">
					<input type="text" name="name" class="form-control" value="<?= isset($detail) ? $detail['name'] : '' ?>">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('kp_no') ?></label>
				<div class="col-md-9">
					<input type="text" name="kp_no" class="form-control" value="<?= isset($detail) ? $detail['kp_no'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
				<div class="col-md-9">
					<input type="text" name="telp_no" class="form-control" value="<?= isset($detail) ? $detail['telp_no'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('fax_no') ?></label>
				<div class="col-md-9">
					<input type="text" name="fax_no" class="form-control" value="<?= isset($detail) ? $detail['fax_no'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('mara_center') ?></label>
				<div class="col-md-9">
					<select name="mara_center" class="form-control">
						<?php foreach ($mara_centers as $key => $mara_center) { ?>
							<option value="<?=$mara_center->id?>" 
								<?= isset($detail) ? ($detail['mara_center_id'] == $mara_center->id ? 'selected' : '') : '' ?>><?=$mara_center->name?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('mara_address') ?></label>
				<div class="col-md-9">
					<textarea name="mara_address" class="form-control"><?= isset($detail) ? $detail['mara_address'] : '' ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('mara_telp_no') ?></label>
				<div class="col-md-9">
					<input type="text" name="mara_telp_no" class="form-control" value="<?= isset($detail) ? $detail['mara_telp_no'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('mara_fax_no') ?></label>
				<div class="col-md-9">
					<input type="text" name="mara_fax_no" class="form-control" value="<?= isset($detail) ? $detail['mara_fax_no'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('innovator_photo') ?></label>
				<div class="col-md-9">
					 <?php if (file_exists(realpath(APPPATH . '../' . PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $detail['photo']) && $detail['photo'] != "") { ?>
	                    <a href="<?= base_url() . PATH_TO_INNOVATOR_PHOTO . $detail['photo'] ?>" class="fancyboxs"><img src="<?= base_url() . PATH_TO_INNOVATOR_PHOTO_THUMB . $detail['photo'] ?>" width="30%"/></a>
	                <?php } ?>
	                <input type="file" class="form-control" name="innovator_photo">
				</div>
			</div>

			<h3>User Detail</h3>
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('email') ?></label>
				<div class="col-md-9">
					<input type="text" name="email" class="form-control" value="<?= isset($detail) ? $detail['email'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('created_at') ?></label>
				<div class="col-md-9">
					<?=$detail['created_at']?>
				</div>
			</div>	

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('password') ?></label>
				<div class="col-md-9">
					<button type="button" data-toggle="collapse" data-target="#password-form" class="btn btn-default"><?= lang('change_password') ?></button>
				</div>
			</div>	

			<div id="password-form" class="collapse">
				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('password') ?></label>
					<div class="col-md-9">
						<input type="password" name="password" class="form-control" placeholder="<?= lang('password') ?>" />
					</div>
				</div>	

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('retype_password') ?></label>
					<div class="col-md-9">
						<input type="password" name="retype_password" class="form-control" placeholder="<?= lang('retype_password') ?>" />
					</div>
				</div>	
			</div>	
			
			<div class="form-group text-center">
				<button type="button" class="btn btn-default flat" onclick="window.history.back()"><?= lang('cancel') ?></button>
				<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
			</div>
		</form>
	</div>
</div>