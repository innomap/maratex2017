<div class="col-md-12">
    <div class="alert alert-info alert-dismissable hide">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>
	<table id="table-user" class="table table-bordered table-striped table-dark">
	    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('email') ?></th>
				<th><?= lang('status') ?></th>
				<th><?= lang('created_at') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
	    </thead>
	    <tbody>
	    	<?php $no = 1; foreach ($users as $user) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $user->email ?></td>
				    <td><?= ($user->status ? "<span class='text-success'>" . lang('active') . "</span>" : "<span class='text-danger'>" . lang('suspended') . "</span>") ?></td>
				    <td><?= $user->created_at ?></td>
				    <td>
				    	<a href="<?=base_url() . PATH_TO_ADMIN . "users/view/" . $user->id?>" class="btn btn-default btn-action btn-edit-group" title="<?= lang('view') ?>">
						    <span class="fa fa-search fa-lg"></span>
						</button>
						<!-- <button type="button" class="btn btn-default btn-action btn-edit-group" title="<?= lang('edit') ?>" data-id="<?= $value['id'] ?>">
						    <span class="fa fa-pencil fa-lg"></span>
						</button>
						<button type="button" class="btn btn-default btn-action btn-delete-group text-red" title="<?= lang('delete') ?>" data-id="<?= $value['id'] ?>"
							data-name="<?= $value['name'] ?>">
						    <span class="fa fa-trash-o fa-lg"></span>
						</button> -->
				    </td>
				</tr>
		    <?php $no++; } ?>
	    </tbody>
	</table>
</div>

<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_group = "<?= lang('delete_group') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>";
</script>