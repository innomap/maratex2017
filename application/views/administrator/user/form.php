<script type="text/javascript">
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_group = "<?= lang('delete_group') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>";
</script>
<div class="col-md-12">
    <div class="col-md-12 text-center">
		<h2><?= lang('form_group') ?></h2>
    </div>
    <div class="col-md-8 col-md-offset-2 content-box">
		<form role="form" class="form-horizontal" id="form-user" action="<?= site_url(PATH_TO_ADMIN.'users/save') ?>" method="POST">
			<input type="hidden" name="id" value="<?= isset($detail) ? $detail->id : '' ?>" />
				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('email') ?></label>
					<div class="col-md-9">
						<input type="text" name="email" class="form-control" value="<?= isset($detail) ? $detail->email : '' ?>" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('status') ?></label>
					<div class="col-md-9">
						<input type="radio" name="status" value="<?=USER_NON_ACTIVE?>" <?= isset($detail) ? (!$detail->status ? 'checked' : '') : '' ?> disabled /> <?=lang('suspended')?>
						<input type="radio" name="status" value="<?=USER_ACTIVE?>" <?= isset($detail) ? ($detail->status ? 'checked' : '') : '' ?> disabled/> <?=lang('active')?>
					</div>
				</div>		

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('created_at') ?></label>
					<div class="col-md-9">
						<?=$detail->created_at?>
					</div>
				</div>	

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('password') ?></label>
					<div class="col-md-9">
						<button type="button" data-toggle="collapse" data-target="#password-form" class="btn btn-info"><?= lang('change_password') ?></button>
					</div>
				</div>	

				<div id="password-form" class="collapse">
					<div class="form-group">
						<label class="col-md-3 control-label"><?= lang('password') ?></label>
						<div class="col-md-9">
							<input type="password" name="password" class="form-control" placeholder="<?= lang('password') ?>" />
						</div>
					</div>	

					<div class="form-group">
						<label class="col-md-3 control-label"><?= lang('retype_password') ?></label>
						<div class="col-md-9">
							<input type="password" name="retype_password" class="form-control" placeholder="<?= lang('retype_password') ?>" />
						</div>
					</div>	
				</div>		
				
				<div class="form-group text-center">
					<button type="button" class="btn btn-default flat" onclick="window.history.back()"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
    </div>
</div>