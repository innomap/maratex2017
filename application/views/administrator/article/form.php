<div class="col-md-12">
    <div class="col-md-12 text-center">
		<h2><?= lang('form_article') ?></h2>
    </div>
    <div class="col-md-8 col-md-offset-2 content-box">
		<form role="form" class="form-horizontal" id="form-article" action="<?= site_url(PATH_TO_ADMIN.'articles/save') ?>" method="POST">
			<input type="hidden" name="id" value="<?= isset($detail) ? $detail->id : '' ?>" />
			<input type="hidden" name="mode" value="<?= $mode ?>" />

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('menu_name') ?></label>
				<div class="col-md-9">
					<input type="text" name="menu_name" class="form-control" value="<?= isset($detail) ? $detail->menu_name : '' ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('title') ?></label>
				<div class="col-md-9">
					<input type="text" name="title" class="form-control" value="<?= isset($detail) ? $detail->title : '' ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('content') ?></label>
				<div class="col-md-9">
					<textarea name="content" rows="10" class="form-control tinymce"><?= isset($detail) ? $detail->content : '' ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('status') ?></label>
				<div class="col-md-9">
					<input type="radio" name="status" value="<?=USER_NON_ACTIVE?>" <?= isset($detail) ? (!$detail->status ? 'checked' : '') : 'checked' ?> /> <?=lang('draft')?>
					<input type="radio" name="status" value="<?=USER_ACTIVE?>" <?= isset($detail) ? ($detail->status ? 'checked' : '') : '' ?> /> <?=lang('published')?>
				</div>
			</div>			
			
			<div class="form-group text-center">
				<button type="button" class="btn btn-default flat" onclick="window.history.back()"><?= lang('cancel') ?></button>
				<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
			</div>
		</form>
    </div>
</div>