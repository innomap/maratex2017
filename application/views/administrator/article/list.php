<div class="col-md-12">
	<div class="row" style="margin-bottom:20px">
		<div class="col-md-12">
			<a href="<?=base_url(). PATH_TO_ADMIN . "articles/add"?>" class="btn btn-success">
				<span class="fa fa-plus"></span> <?= lang('new_article') ?>
			</a>
	    </div>
	</div>

    <div class="alert alert-info alert-dismissable hide">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>

	<table id="table-article" class="table table-bordered table-striped table-dark">
	    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('menu_name') ?></th>
				<th><?= lang('title') ?></th>
				<th><?= lang('status') ?></th>
				<th><?= lang('created_at') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
	    </thead>
	    <tbody>
	    	<?php $no = 1; foreach ($articles as $article) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $article->menu_name ?></td>
				    <td><?= $article->title ?></td>
				    <td><?= ($article->status ? "<span class='text-success'>" . lang('published') . "</span>" : "<span class='text-danger'>" . lang('draft') . "</span>") ?></td>
				    <td><?= $article->created_at ?></td>
				    <td>
						<a href="<?=base_url(). PATH_TO_ADMIN . "articles/edit/" . $article->id?>" class="btn btn-default btn-action btn-edit-group" title="<?= lang('edit') ?>">
						    <span class="fa fa-pencil fa-lg"></span>
						</a>
						<button type="button" class="btn btn-default btn-action btn-delete-article text-red" title="<?= lang('delete') ?>" data-id="<?= $article->id ?>"
							data-name="<?= $article->title ?>">
						    <span class="fa fa-trash-o fa-lg"></span>
						</button>
				    </td>
				</tr>
		    <?php $no++; } ?>
	    </tbody>
	</table>
</div>

<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_article = "<?= lang('delete_article') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>";
</script>