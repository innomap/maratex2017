<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>

<div class="col-md-12 container">
	<div class="col-md-8 col-md-offset-2">
		<div class="col-md-6 col-md-offset-3">
			<div class="col-md-offset-1 col-md-11 no-padding" style="margin-top: 10%;">
				<a href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/img/new-logo-maratex.png" class="img-responsive"></a>
			</div>
		</div>	
		<div class="col-md-12 text-center">
			<h2>Administrator Login</h2>
		</div>

		<div class="col-md-6 col-md-offset-3 login-box bg-grey-2">
			<div class="alert alert-info alert-dismissable hide">
			    <i class="fa fa-info"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			    <?= $alert ?>
			</div>
			<form id="form-login" action="<?= base_url().PATH_TO_ADMIN.'accounts/login_auth' ?>" method="POST">
				<div class="form-group">
					<label><?= lang('email') ?></label>
					<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>">
				</div>

				<div class="form-group">
					<label><?= lang('password') ?></label>
					<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
				</div>

				<div class="col-md-12 text-center">
					<input type="submit" class="btn btn-success rounded" name="btn_login" value="Log In">
				</div>
			</form>
		</div>
	</div>
</div>