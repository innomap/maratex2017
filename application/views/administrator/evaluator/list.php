<div class="col-md-12">
	<div class="row" style="margin-bottom:20px">
		<div class="col-md-12">
			<a href="<?=base_url(). PATH_TO_ADMIN . "evaluators/add"?>" class="btn btn-success">
				<span class="fa fa-plus"></span> <?= lang('new_evaluator') ?>
			</a>
	    </div>
	</div>

    <div class="alert alert-info alert-dismissable hide">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>

	<table id="table-evaluator" class="table table-bordered table-striped table-dark">
	    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('name') ?></th>
				<th><?= lang('address') ?></th>
				<th><?= lang('telp_no') ?></th>
				<th><?= lang('email') ?></th>
				<th><?= lang('status') ?></th>
				<th><?= lang('created_at') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
	    </thead>
	    <tbody>
	    	<?php $no = 1; foreach ($evaluators as $evaluator) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $evaluator['name'] ?></td>
				    <td><?= $evaluator['address'] ?></td>
				    <td><?= $evaluator['telp_no'] ?></td>
				    <td><?= $evaluator['email'] ?></td>
				    <td><?= ($evaluator['status'] ? "<span class='text-success'>" . lang('active') . "</span>" : "<span class='text-danger'>" . lang('suspended') . "</span>") ?></td>
				    <td><?= $evaluator['created_at'] ?></td>
				    <td>
						<a href="<?=base_url(). PATH_TO_ADMIN . "evaluators/edit/" . $evaluator['id']?>" class="btn btn-default btn-action btn-edit-group" title="<?= lang('edit') ?>">
						    <span class="fa fa-pencil fa-lg"></span>
						</a>
						<button type="button" class="btn btn-default btn-action btn-delete-evaluator text-red" title="<?= lang('delete') ?>" data-id="<?= $evaluator['user_id'] ?>"
							data-name="<?= $evaluator['name'] ?>">
						    <span class="fa fa-trash-o fa-lg"></span>
						</button>
				    </td>
				</tr>
		    <?php $no++; } ?>
	    </tbody>
	</table>
</div>

<script type="text/javascript">
	var alert = "<?=$alert?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_evaluator = "<?= lang('delete_evaluator') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>";
</script>