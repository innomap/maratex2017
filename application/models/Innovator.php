<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Innovator extends MY_Model {
	public $_table = 'innovator';
	public $primary_key = 'user_id';
	public $soft_delete = TRUE;
	public $before_create = array('created_at');
	public $before_update = array('updated_at');
	public $validate = array(array('field' => 'name',
	                     'label' => 'name',
	                     'rules' => 'trim|required',
	                     'errors' => array('required' => 'The name field is required.')));

	public $belongs_to = array('user' => array('model' => 'user',
	                                 'primary_key' => 'user_id'),
								'mara_center' => array('model' => 'mara_center',
	                                 'primary_key' => 'mara_center_id'));

	function __construct() {
		parent::__construct();
	}

	function get_with_user($where = NULL){
		$this->db->select('u.*, i.*, mc.name as mara_center');
		$this->db->from('innovator as i');
		$this->db->join('user as u', 'u.id = i.user_id');
		$this->db->join('mara_center as mc', 'mc.id = i.mara_center_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('i.deleted', false);
		$this->db->where('u.deleted', false);

		$q = $this->db->get();
		return $q->result_array();
	}
}