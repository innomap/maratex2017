<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class User extends MY_Model {
	public $_table = 'user';
	public $soft_delete = TRUE;
	public $before_create = array('created_at','hash_password');
	public $before_update = array('updated_at','hash_password');
	public $validate = array(array('field' => 'email',
	                     'label' => 'email',
	                     'rules' => 'trim|required|valid_email',
	                     'errors' => array('is_unique' => 'Email address already in use.')),
	                    array('field' => 'password',
	                     'label' => 'password',
	                     'rules' => 'trim|required',
	                     'errors' => array('required' => 'The password field is required.')));

	function __construct() {
		parent::__construct();
	}

	function hash_password($row) {
		if(isset($row['password'])){
			$row['password'] = $this->get_hash($row['email'],$row['password']);
		}
		return $row;
	}

	function get_hash($email, $password) {
		return md5($email.':'.$password);
	}

	function auth($email, $password){
		$user = $this->get_by('email', $email);
		
		if (is_null($user)) {
			return FALSE;
		}

		if($this->check_password($email, $password, $user->password)){
			return $user;
		}

		return FALSE;
	}

	private function check_password($email, $password, $hash) {
		$password = $this->user->get_hash($email, $password);
		if($password == $hash){
			return true;
		}else{
			return false;
		}
	}
	
	function is_email_exist($email, $id = 0){
		$where = array();
		$where['email'] = $email;
		if($id != 0){
			$where['id != '] = $id;
		}
		$user = $this->get_by($where);
		if($user != NULL){
			return true;
		}else{
			return false;
		}
	}
}