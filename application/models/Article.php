<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Article extends MY_Model {
	public $_table = 'article';
	public $soft_delete = TRUE;
	public $before_create = array('created_at');
	public $before_update = array('updated_at');
	public $validate = array(array('field' => 'menu_name',
	                     'label' => 'menu_name',
	                     'rules' => 'trim|required',
	                     'errors' => array('required' => 'The menu name field is required.')));

	function __construct() {
		parent::__construct();
	}
}