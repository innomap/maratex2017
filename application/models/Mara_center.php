<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Mara_center extends MY_Model {
	public $_table = 'mara_center';
	public $soft_delete = TRUE;
	public $before_create = array('created_at');
	public $before_update = array('updated_at');
	public $validate = array(array('field' => 'name',
	                     'label' => 'name',
	                     'rules' => 'trim|required',
	                     'errors' => array('required' => 'The name field is required.')));

	function __construct() {
		parent::__construct();
	}
}