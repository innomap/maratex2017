<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Application_category extends MY_Model {
	public $_table = 'application_category';
	public $soft_delete = TRUE;
	public $before_create = array('created_at');
	public $before_update = array('updated_at');
	public $validate = array(array('field' => 'name',
	                     'label' => 'name',
	                     'rules' => 'trim|required',
	                     'errors' => array('required' => 'The name field is required.')));

	function __construct() {
		parent::__construct();
	}
}