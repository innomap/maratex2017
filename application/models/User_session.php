<?php

class User_session extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_user() {
		return $this->session->userdata('maratex2017_user');
	}

	function set_user($data) {
		$this->session->set_userdata('maratex2017_user', $data);
	}

	function clear() {
		$this->session->sess_destroy();
	}

	function get_admin() {
		return $this->session->userdata('maratex2017_admin');
	}

	function set_admin($data) {
		$this->session->set_userdata('maratex2017_admin', $data);
	}

	function get_evaluator() {
		return $this->session->userdata('maratex2017_evaluator');
	}

	function set_evaluator($data) {
		$this->session->set_userdata('maratex2017_evaluator', $data);
	}
}