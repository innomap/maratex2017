<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Application extends MY_Model {
	public $_table = 'application';
	public $soft_delete = TRUE;
	public $before_create = array('created_at');
	public $before_update = array('updated_at');

	public $belongs_to = array('user' => array('model' => 'user',
	                                 'primary_key' => 'user_id'),
								'application_category' => array('model' => 'application_category',
	                                 'primary_key' => 'application_category_id'));

	public $has_many = array('experts' => array('model' => 'application_team_expert',
	                                 'primary_key' => 'application_id'),
								'pictures' => array('model' => 'application_picture',
	                                 'primary_key' => 'application_id'));

	function __construct() {
		parent::__construct();
	}
}