<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Evaluator extends MY_Model {
	public $_table = 'evaluator';
	public $primary_key = 'user_id';
	public $soft_delete = TRUE;
	public $before_create = array('created_at');
	public $before_update = array('updated_at');
	public $validate = array(array('field' => 'name',
	                     'label' => 'name',
	                     'rules' => 'trim|required',
	                     'errors' => array('required' => 'The name field is required.')));

	function __construct() {
		parent::__construct();
	}

	function get_with_user($where = NULL){
		$this->db->select('u.*, e.*');
		$this->db->from('evaluator as e');
		$this->db->join('user as u', 'u.id = e.user_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('e.deleted', false);
		$this->db->where('u.deleted', false);

		$q = $this->db->get();
		return $q->result_array();
	}
}