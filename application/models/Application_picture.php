<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Application_picture extends MY_Model {
	public $_table = 'application_picture';
	public $soft_delete = TRUE;
	public $before_create = array('created_at');
	public $before_update = array('updated_at');

	public $belongs_to = array('application' => array('model' => 'application',
	                                 'primary_key' => 'application_id'));

	function __construct() {
		parent::__construct();
	}
}