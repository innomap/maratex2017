<?php
    $ol_user = $this->user_session->get_evaluator();
?>
<html>
    <head>
        <base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | Anugerah MARA 2017</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>bootstrapValidator.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>font-awesome.min.css" />
        <link href="<?= ASSETS_CSS ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>site.css" rel="stylesheet" type="text/css" />
        {{styles}}
    </head>
    <body class="default <?= (!$ol_user ? 'landing-page' : '') ?>">
        <?php if($ol_user){ ?>
            <div class="header">
                <nav class="menu navbar navbar-default">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar-header" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="<?= $this->menu == "application" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_EVALUATOR.'applications' ?>">Submissions</a></li>
                            <li class="<?= $this->menu == "profile" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_EVALUATOR.'profile' ?>">Edit Profile</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right logout-btn">
                            <li><a href="<?= base_url().'accounts/logout' ?>"><span class="fa fa-sign-out"></span>Logout</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        <?php } ?>
        
        {{content}}
        <script type="text/javascript">
            var baseUrl = "<?=base_url()?>";
            var adminUrl = "<?=base_url().PATH_TO_ADMIN?>";
        </script>
         <!-- jQuery -->
        <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="<?= ASSETS_JS ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
        <script src="<?= ASSETS_JS ?>bootbox.min.js" type="text/javascript"></script>
        <script>
            tinymce.init({
              selector: '.tinymce',
              height: 500,
              theme: 'modern',
              plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
              ],
              toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
              toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
              image_advtab: true
            });
        </script>
        {{scripts}}
    </body>
</html>