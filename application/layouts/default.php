<?php
    $ol_user = $this->user_session->get_user();
?>
<html>
    <head>
        <base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | Anugerah Inovasi MARA 2017</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>bootstrapValidator.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>font-awesome.min.css" />
        <link href="<?= ASSETS_CSS ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>site.css" rel="stylesheet" type="text/css" />
        {{styles}}
    </head>
    <body class="default <?= ($this->menu == 'site' ? 'landing-page' : 'bg-grey') ?>">
        <?php if($ol_user){ ?>
            <div class="header">
                <nav class="menu navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar-header" class="collapse navbar-collapse">
                        <?php if($this->menu != "dashboard"){ ?>
                            <ul class="nav navbar-nav">
                                <li class="<?= $this->menu == "application" ? "active" : "" ?>"><a href="<?= base_url().'applications' ?>">Penyertaan</a></li>
                                <li class="<?= $this->menu == "profile" ? "active" : "" ?>"><a href="<?= base_url().'profile' ?>">Edit Profil</a></li>
                                <!--<li><a href="<?= base_url().'assets/attachment/'.BRIEF_MARATEX_FILENAME ?>" target="_blank">Muat Turun</a></li>-->
                            </ul>
                        <?php } ?>
                        <ul class="nav navbar-nav navbar-right logout-btn">
                            <li>
                                <a href="<?= base_url().'accounts/logout' ?>"><span class="fa fa-sign-out"></span>Logout</a>
                            </li>
                        </ul> 
                    </div>
                </nav>
            </div>
        <?php } ?>
        
        <div class="col-xs-12 col-sm-12 col-md-12 content no-padding">
            {{content}}
        </div>
        
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding footer">
            <?php /*if($this->menu == "dashboard" || $this->menu == 'account' || $this->menu == 'site'){ ?>
                <div class="foot-menu">
                    <nav class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-footer" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar-footer" class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li class="<?= ($this->submenu == 'home' ? 'active' : '') ?>"><a href="<?= base_url().'login' ?>">Borang Penyertaan</a></li>
                                <li class="<?= ($this->submenu == 'about_us' ? 'active' : '') ?>"><a href="<?= base_url().'site/about_us' ?>">Pengenalan</a></li>
                                <li class="<?= ($this->submenu == 'news' ? 'active' : '') ?>"><a href="<?= base_url().'site/news' ?>">Berita</a></li>
                                <li class="<?= ($this->submenu == 'competition' ? 'active' : '') ?>"><a href="<?= base_url().'site/competition' ?>">Pertandingan</a></li>
                                <li class="<?= ($this->submenu == 'faq' ? 'active' : '') ?>"><a href="<?= base_url().'site/faq' ?>">Soalan Lazim</a></li>
                                <li class="<?= ($this->submenu == 'contact_us' ? 'active' : '') ?>"><a href="<?= base_url().'site/contact_us' ?>">Hubungi Sekretariat</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            <?php }*/ ?>
                <!-- <div class="navbar navbar-inverse navbar-fixed-bottom">
                    <div class="container">
                        <div class="navbar-collapse collapse" id="footer">
                            <ul class="nav navbar-nav">
                                <li><a href="<?= base_url().'login' ?>">Borang Penyertaan</a></li>
                                <li><a href="#">Pengenalan</a></li>
                                <li><a href="#">Borang Penyertaan</a></li>
                                <li><a href="#">Berita</a></li>
                                <li><a href="#">Pertandingan</a></li>
                                <li><a href="#">Soalan Lazim</a></li>
                                <li><a href="#">Sekretaria</a></li></li>
                            </ul>
                        </div>

                    <div class="navbar-footer">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </div>
                    <div class="foot-content text-center" style="color: #777;font-size: 8pt;">
                        <img src="<?= base_url().ASSETS_IMG."logo-mara.png" ?>" width="20px">
                        <span>Organised by MARA Innovation and Research Unit in collaboration with Yayasan Inovasi Malaysia</span>
                        <img src="<?= base_url().ASSETS_IMG."logo-mini.png" ?>" width="22px">
                    </div>
                </div> -->
                <!-- <div class="foot-content text-center">
                    <img src="<?= base_url().ASSETS_IMG."logo-mara.png" ?>" width="27px">
                    <span>Organised by MARA Innovation and Research Unit in collaboration with Yayasan Inovasi Malaysia</span>
                    <img src="<?= base_url().ASSETS_IMG."logo-mini.png" ?>" width="30px">
                </div> -->
            </div>
            
        <script type="text/javascript">
            var baseUrl = "<?=base_url()?>";
        </script>
        <!-- jQuery -->
        <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="<?= ASSETS_JS ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="<?= ASSETS_JS ?>bootbox.min.js" type="text/javascript"></script>
        {{scripts}}
    </body>
</html>