<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Applications extends Common {

	function __construct() {
		parent::__construct();

		$this->layout = DEFAULT_LAYOUT;
		$this->load->model('innovator');
        $this->load->model('application_category');
        $this->load->model('application');
        $this->load->model('application_status_history');
        $this->load->model('application_picture');
        $this->load->model('application_team_expert');

        $this->load->library('upload');

		$this->lang->load('application',$this->language);

		$this->title = "Applications";
		$this->menu = "application";

        $this->scripts[] = 'jquery.form';
		$this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
		$this->scripts[] = 'site/application';

		$this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
        $this->load->helper('mystring_helper');

    	$data['alert'] = $this->session->flashdata('alert');
    	$data['applications'] = $this->application->get_many_by('user_id', $this->userdata['id']);
        $data['status'] = unserialize(APPLICATION_STATUS);
        $this->load->view('application/list', $data);
    }

    public function add(){
    	$data['alert'] = $this->session->flashdata('alert');
        $data['alert_dialog'] = $this->session->flashdata('alert_dialog');
    	$data['action_url'] = 'store';
    	$data['view_mode'] = 0;
    	$data['person'] = $this->innovator->with('mara_center')->with('user')->get($this->userdata['id']);

        $categories = $this->application_category->get_many_by('parent_id', NULL);
        $sub_categories = $this->get_subcategories($categories);

        $data['categories'] = $sub_categories;
        $data['category_content'] = $this->draw_category_list($sub_categories);
        $data['project_readiness_content'] = $this->draw_project_readiness_list(unserialize(PROJECT_READINESS));

    	$this->load->view('application/form',$data);
    }

    public function edit($id, $view_mode = 0){
        $data['alert'] = $this->session->flashdata('alert');
        $data['alert_dialog'] = $this->session->flashdata('alert_dialog');
        $data['action_url'] = 'update';
        $data['view_mode'] = $view_mode;
        $data['person'] = $this->innovator->with('mara_center')->with('user')->get($this->userdata['id']);

        $categories = $this->application_category->get_many_by('parent_id', NULL);
        $sub_categories = $this->get_subcategories($categories);

        $app = $this->application->with('pictures')->with('experts')->get($id);

        $data['categories'] = $sub_categories;
        $data['category_content'] = $this->draw_category_list($sub_categories,0,$app->application_category_id);
        $data['project_readiness_content'] = $this->draw_project_readiness_list(unserialize(PROJECT_READINESS),0 ,$app->readiness_opt);

        $data['app'] = $app;

        $this->load->view('application/form',$data);   
    }

    public function view($id){
        $this->edit($id, 1);
    }

    private function get_subcategories($parent, $level = 0){
        foreach ($parent as $key => $value) {
            $has_child = $this->application_category->get_many_by('parent_id', $value->id);
            if($has_child){
                $level++;
                $parent[$key]->sub_menu = $this->get_subcategories($has_child);
            }
        }

        return $parent;
    }

    private function draw_category_list($categories, $level = 0, $selected_id = 0){
        $content = '<div class="panel-group" id="accordion'.$categories[0]->id.'" role="tablist" aria-multiselectable="true">';
        foreach ($categories as $key=>$item) {
            if (isset($item->sub_menu)) {
                $content .= '<div class="panel panel-transparent">';
                $content .=     '<div class="panel-heading" role="tab" id="heading'.$item->id.'">';
                $content .=         '<h4 class="panel-title">';
                $content .=             '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$item->id.'" aria-expanded="true" aria-controls="collapse'.$item->id.'">';
                $content .=                 "(+) ".$item->name;
                $content .=              '</a>';
                $content .=          '</h4>';
                $content .=     '</div>';
                $content .=     '<div id="collapse'.$item->id.'" class="panel-collapse collapse '.($level == 0 && $key == 0 ? 'in' : '').'" role="tabpanel" aria-labelledby="#heading'.$item->id.'">';
                $content .=         '<div class="panel-body">';
                $content .=         $this->draw_category_list($item->sub_menu, 1, $selected_id); // here is the recursion
                $content .=         '</div>';
                $content .=     '</div>';
                $content .=   '</div>';
            }else{
                $content .= '<div class="radio">';
                $content .=     '<label>';
                $content .=         '<input type="radio" name="application_category_id" value="'.$item->id.'" '.($selected_id == $item->id ? 'checked' : '').'>'.$item->name;
                $content .=     '</label>';
                $content .=     '<label><i>'.$item->desc.'</i></label>';
                $content .= '</div>';
            }
        }
        $content .= '</div>';

        return $content;
    }

    public function store(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        $data_application = $this->generate_data_application($postdata);
        
        if ($id = $this->application->insert($data_application)) {
            //save picture
            $this->save_pictures($_FILES, $id);

            //save team member
            $this->save_team_member($id, $postdata);

            //save additional attachment
            $this->save_additional_attachment($_FILES, $id, $postdata);

            //save history
            $this->application_status_history->insert(array('application_id' => $id, 'round_id' => 1, 'status' => APPLICATION_STATUS_DRAFT, 'user_id' => $this->userdata['id']));
            if (isset($_POST['btn_send_for_approval'])) {
                $this->application->update($id, array('status' => APPLICATION_STATUS_SENT_APPROVAL, 'submission_date' => date('Y-m-d H:i:s')));
                $this->application_status_history->insert(array('application_id' => $id, 'round_id' => 1, 'status' => APPLICATION_STATUS_SENT_APPROVAL, 'user_id' => $this->userdata['id']));
            }

            if (isset($_POST['btn_send_for_approval'])) {
                $this->session->set_flashdata('alert_dialog', $id);
                $redirect_url = base_url() . 'applications';
            } else {
                $redirect_url = base_url() . 'applications/edit/' . $id;
            }

            echo json_encode(array('redirect_url' => $redirect_url));
        } else {
            echo json_encode(array('redirect_url' => base_url() . 'applications'));
        }
    }

    private function generate_data_application($postdata){
        $data_application = array(
            "user_id" => $postdata['user_id'],
            "application_category_id" => (isset($postdata['application_category_id']) ? $postdata['application_category_id'] : NULL),
            "project_title" => $postdata['project_title'],
            "project_description" => $postdata['project_description'],
            "potential" => $postdata['potential'],
            "significant" => $postdata['significant'],
            "relevancy" => $postdata['relevancy'],
            "outcome" => $postdata['outcome'],
            "sustainability" => $postdata['sustainability'],
            "readiness_opt" => $postdata['readiness_opt'],
            "readiness" => $postdata['readiness'],
            "sustainability" => $postdata['sustainability'],
            "readiness_opt" => (isset($postdata['readiness_opt']) ? $postdata['readiness_opt'] : NULL),
            "readiness" => $postdata['readiness'],
            "advisor_name" => $postdata['advisor'],
            "advisor_kp_no" => $postdata['advisor_kp_no'],
            "advisor_email" => $postdata['advisor_email'],
            "advisor_telp_no" => $postdata['advisor_telp_no'],
            "advisor_fax_no" => $postdata['advisor_fax_no'],
            "headquarters_approval" => (isset($postdata['headquarters_approval']) ? $postdata['headquarters_approval'] : 0),
            "participant_approval" => (isset($postdata['participant_approval']) ? $postdata['participant_approval'] : 0),
            "round_id" => 1,
            "status" => APPLICATION_STATUS_DRAFT
            );

        //$data_application['advisor_photo'] = "";

        return $data_application;
    }

    private function postdata() {
        if ($post = $this->input->post()) {
            return $post;
        }
        redirect('applications');
    }

    private function draw_project_readiness_list($list, $level = 0, $selected_id = 0){
        $content = '';
        foreach ($list as $item) {
            $content .= '<div class="radio '.($level == 0 ? '' : 'padding-left-40').'">';
            $content .=     '<label>';
            $content .=         ($level == 0 ? '<b>' : '');
            $content .=             '<input type="radio" name="readiness_opt" '.(isset($item['child']) ? 'disabled' : '').' '.($selected_id == $item['id'] ? 'checked' : '').' value="'.$item['id'].'">'.$item['name'];
            $content .=         ($level == 0 ? '</b>' : '');
            $content .=     '</label>';
            $content .= '</div>';
            if (isset($item['child'])) {
                $content .= $this->draw_project_readiness_list($item['child'], 1, $selected_id);
            }
        }

        return $content;
    }

    private function generate_filename($id, $filename) {
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id . "_" . rand() . "_" . $attachment_name;

        return $retval;
    }

    private function _upload($name, $attachment, $upload_path, $thumb_path = NULL, $is_file = 0) {
        $config['file_name'] = $name;
        $config['upload_path'] = $upload_path;
        if ($is_file == 0) {
            $config['allowed_types'] = 'png|jpg|gif|bmp|jpeg';
        } else {
            $config['allowed_types'] = 'png|jpg|gif|bmp|jpeg|pdf|doc|docx';
        }

        $config['remove_spaces'] = TRUE;

        $this->upload->initialize($config);
        if (!$this->upload->do_upload($attachment, true)) {
            echo $this->upload->display_errors();
            return false;
        } else {
            $upload_data = $this->upload->data();
            if ($thumb_path != NULL) {
                $this->create_thumbnail($upload_data['image_width'], $upload_data['image_height'], $upload_path, $upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }

    private function create_thumbnail($image_width, $image_height, $upload_path, $file_name, $new_path) {
        $this->load->library('image_lib');

        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path . $file_name;
            $config['new_image'] = $new_path . $file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
                return false;
            }
        } else {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path . $file_name;
            $config['new_image'] = $new_path . $file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function save_team_member($id, $postdata){
        foreach ($postdata['team_member_id'] as $key => $value) {
            if(isset($postdata['i_deleted_team_member_'.$key])){
                $data_member = array('application_id' => $id,
                                'name' => $postdata['team_member_'.$key],
                                'kp_no' => $postdata['team_member_kp_'.$key],
                                'telp_no' => $postdata['team_member_telp_'.$key]);

                if(isset($_FILES['team_member_pic_'.$key])){
                    if($_FILES['team_member_pic_'.$key]['name'] != NULL){
                        if($postdata['h_team_member_pic_'.$key] != ""){
                            $this->_remove($postdata['h_team_member_pic_'.$key],PATH_TO_TEAM_MEMBER_PHOTO);
                            $this->_remove($postdata['h_team_member_pic_'.$key],PATH_TO_TEAM_MEMBER_PHOTO);
                        }
                        $photo_filename = $this->generate_filename($id,$_FILES['team_member_pic_'.$key]['name']);
                        $uploaded_photo = $this->_upload($photo_filename,'team_member_pic_'.$key,PATH_TO_TEAM_MEMBER_PHOTO, PATH_TO_TEAM_MEMBER_PHOTO_THUMB);
                        
                        $data_member['photo'] = $uploaded_photo;
                    }
                }
                if($postdata['team_member_id'][$key] > 0){
                    $this->application_team_expert->update($postdata['team_member_id'][$key], $data_member);
                }else{
                    $this->application_team_expert->insert($data_member);
                }
            }else{
                if($postdata['team_member_id'][$key] > 0){
                    $team_member = $this->application_team_expert->get($postdata['team_member_id'][$key]);
                    $this->_remove($team_member->photo,PATH_TO_TEAM_MEMBER_PHOTO);
                    $this->application_team_expert->delete($postdata['team_member_id'][$key]);
                }
            }
        }
    }

    public function update(){        
        $this->layout = FALSE;

        if(isset($_POST['btn_send_for_approval'])){
            $status = APPLICATION_STATUS_SENT_APPROVAL;
        }else{
            $status = APPLICATION_STATUS_DRAFT;
        }
        $postdata = $this->postdata();

        if($postdata['project_title'] != "" || $postdata['project_description'] != ""){
            $data_application = $this->generate_data_application($postdata);
            $id = $postdata['id'];
            if($this->application->update($postdata["id"], $data_application)){
                //save team member
                $this->save_team_member($id, $postdata);

                //save picture
                if(isset($postdata['deleted_picture'])){
                    $deleted_pict = $postdata['deleted_picture'];
                    $deleted_pict_name = $postdata['deleted_picture_name'];
                    foreach ($deleted_pict as $key => $value) {
                        if($this->application_picture->delete($value)){
                            $this->_remove($deleted_pict_name[$key],PATH_TO_APPLICATION_PICTURE);
                            $this->_remove($deleted_pict_name[$key],PATH_TO_APPLICATION_PICTURE_THUMB);
                        }
                    }
                }
                
                //save additional attachment
                $this->save_additional_attachment($_FILES, $id, $postdata);

                //save picture
                $this->save_pictures($_FILES, $id);

                if($status == APPLICATION_STATUS_SENT_APPROVAL){
                    $round = 1;
                    $this->application->update($id, array('status' => APPLICATION_STATUS_SENT_APPROVAL, 'submission_date' => date('Y-m-d H:i:s')));
                    $this->application_status_history->insert(array('application_id' => $id, 'round_id' => 1, 'status' => APPLICATION_STATUS_SENT_APPROVAL, 'user_id' => $this->userdata['id']));

                    $this->session->set_flashdata('alert',lang('application_submitted_msg'));
                    $this->session->set_flashdata('alert_dialog',base_url()."innovations/generate_pdf/".$id);
                }else{
                    $this->session->set_flashdata('alert','Application has been updated.');
                }                
            }else{
                $this->session->set_flashdata('alert','Appication cannot be updated.');
            }
        }else{
            $this->session->set_flashdata('alert','Some fields are required.');
        }

        if (isset($_POST['btn_send_for_approval'])) {
            $this->session->set_flashdata('alert_dialog', $id);
            $redirect_url = base_url() . 'applications';
        } else {
            $redirect_url = base_url() . 'applications/edit/' . $id;
        }

        echo json_encode(array('redirect_url' => $redirect_url));
    }

    private function _remove($file_name, $path) {
        if (file_exists(realpath(APPPATH . '../' . $path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./" . $path . "/" . $file_name);
        }
    }

    private function save_pictures($files, $id){
        $pict_num = count($files);
        for ($i = 0; $i <= $pict_num; $i++) {
            if (isset($files['picture_' . $i])) {
                if ($files['picture_' . $i]['name'] != NULL) {
                    $filename = $this->generate_filename($id, $files['picture_' . $i]['name']);
                    $uploaded_picture = $this->_upload($filename, 'picture_' . $i, PATH_TO_APPLICATION_PICTURE, PATH_TO_APPLICATION_PICTURE_THUMB);
                    $this->application_picture->insert(array('application_id' => $id, 'name' => $uploaded_picture));
                }
            }
        }
    }

    private function save_additional_attachment($files, $id, $postdata){
        if(isset($files['headquarters_approval_attachment'])){
            if($files['headquarters_approval_attachment']['name'] != NULL){
                if($postdata['h_headquarters_approval_attachment'] != ""){
                    $this->_remove($postdata['h_headquarters_approval_attachment'],PATH_TO_HEAD_APPROVAL_ATTACHMENT);
                }
                $attach_filename = $this->generate_filename($id,$files['headquarters_approval_attachment']['name']);
                $uploaded_attach = $this->_upload($attach_filename,'headquarters_approval_attachment',PATH_TO_HEAD_APPROVAL_ATTACHMENT, NULL, 1);
                $attachment_data['headquarters_approval_attachment'] = $uploaded_attach;
            }
        }

        if(isset($files['participant_approval_attachment'])){
            if($files['participant_approval_attachment']['name'] != NULL){
                if($postdata['h_participant_approval_attachment'] != ""){
                    $this->_remove($postdata['h_participant_approval_attachment'],PATH_TO_PARTICIPANT_APPROVAL_ATTACHMENT);
                }
                $attach_filename = $this->generate_filename($id,$files['participant_approval_attachment']['name']);
                $uploaded_attach = $this->_upload($attach_filename,'participant_approval_attachment',PATH_TO_PARTICIPANT_APPROVAL_ATTACHMENT, NULL, 1);
                $attachment_data['participant_approval_attachment'] = $uploaded_attach;
            }
        }

        if(isset($files['advisor_photo'])){
            if($files['advisor_photo']['name'] != NULL){
                if($postdata['h_advisor_photo'] != ""){
                    $this->_remove($postdata['h_advisor_photo'],PATH_TO_ADVISOR_PHOTO);
                    $this->_remove($postdata['h_advisor_photo'],PATH_TO_ADVISOR_PHOTO_THUMB);
                }
                $attach_filename = $this->generate_filename($id,$files['advisor_photo']['name']);
                $uploaded_attach = $this->_upload($attach_filename,'advisor_photo',PATH_TO_ADVISOR_PHOTO, PATH_TO_ADVISOR_PHOTO_THUMB, 0);
                $attachment_data['advisor_photo'] = $uploaded_attach;
            }
        }

        if(isset($attachment_data)){
            $this->application->update($id, $attachment_data);
        }
    }

    function delete($id) {
        $this->layout = FALSE;
        if ($this->application->delete($id)) {
                $this->session->set_flashdata('alert', 'Application has been deleted.');
        } else {
                $this->session->set_flashdata('alert', 'Application can not be deleted.');
        }

        redirect(base_url() . 'applications');
    }
}