<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_EVALUATOR.'/Common.php');
class Applications extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Applications";
		$this->menu = "application";
/*
		$this->load->model('application');
		$this->load->model('application_status_history');
        $this->load->model('evaluation');
        $this->load->model('evaluation_detail');
        $this->load->model('evaluator');
        $this->load->model('evaluation_focus_model');
        $this->load->model('evaluation_criteria');
        $this->load->model('innovator');*/

		$this->lang->load(PATH_TO_ADMIN. 'innovator',$this->language);

        $this->scripts[] = PATH_TO_EVALUATOR.'application';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['applications'] = array();
    	//$data['status'] = unserialize(APPLICATION_STATUS_EVALUATOR);
		$this->load->view(PATH_TO_EVALUATOR.'application/list',$data);
    }
}

?>