<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_EVALUATOR.'/Common.php');
class Profile extends Common {

    function __construct() {
        parent::__construct();

        $this->load->model(array('evaluator', 'user'));

        $this->title = "Edit Profile";
        $this->menu = "profile";

        $this->lang->load(PATH_TO_ADMIN.'evaluator',$this->language);

        $this->scripts[] = PATH_TO_ADMIN.'evaluator';
    }

    public function index(){
        $evaluator = $this->evaluator->get_with_user(array('e.user_id' => $this->userdata['id']));
        $data['detail'] = $evaluator[0];
        $this->load->view(PATH_TO_EVALUATOR.'profile/form', $data); 
    }

    function save(){
        $postdata = $this->postdata();
        $user_postdata = array("email" => $postdata['email']);
        $evaluator_postdata = array("name"    => $postdata['name'],
                                    "address" => $postdata['address'],
                                    "telp_no" => $postdata['telp_no']);

        $success = false;
        if($postdata["password"] != ""){
            $user_postdata["password"] = $postdata['password'];
        }
        $success1 = $this->user->update($this->userdata['id'], $user_postdata, true);
        if($success1){
            $success = $this->evaluator->update($this->userdata['id'], $evaluator_postdata);
        }

        if($success){
            $this->session->set_flashdata('alert','Profile successfully updated');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

        redirect(base_url().PATH_TO_EVALUATOR.'profile');
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_EVALUATOR.'profile');
    }
}
