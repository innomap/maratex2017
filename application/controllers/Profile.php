<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Profile extends Common {

    function __construct() {
        parent::__construct();

        $this->load->model(array('innovator', 'user', 'mara_center'));

        $this->title = "Edit Profile";
        $this->menu = "profile";

        $this->lang->load(PATH_TO_ADMIN.'innovator',$this->language);

        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = 'site/profile';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
        $innovator = $this->innovator->get_with_user(array('i.user_id' => $this->userdata['id']));
        $data['detail'] = $innovator[0];
        $data['mara_centers'] = $this->mara_center->get_all();
        $this->load->view('profile/form', $data); 
    }

    function save(){
        //$this->layout = FALSE;
        $status = 0;

        $postdata = $this->postdata();

        if($postdata['name'] != ""){
            $data = array('email' => $postdata['email']);
            if($postdata['password'] != ""){
                $data['password'] = $postdata['password'];
            }

            if($this->user->update($this->userdata['id'], $data, true)){
                $this->session->set_flashdata('alert','Data has been saved.');
            }else{
                $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
            }
            
            $post_innovator = array( "name" => $postdata['name'],
                                "kp_no" => $postdata['kp_no'],
                                "telp_no" => $postdata['telp_no'],
                                "fax_no" => $postdata['fax_no'],
                                "mara_center_id" => $postdata['mara_center'],
                                "mara_address" => $postdata['mara_address'],
                                "mara_telp_no" => $postdata['mara_telp_no'],
                                "mara_fax_no" => $postdata['mara_fax_no']);
            //save photo
            if(isset($_FILES['innovator_photo'])){
                if($_FILES['innovator_photo']['name'] != NULL){
                    $photo_filename = $this->generate_filename($this->userdata['id'],$_FILES['innovator_photo']['name']);
                    $uploaded_photo = $this->_upload($photo_filename,'innovator_photo',PATH_TO_INNOVATOR_PHOTO, PATH_TO_INNOVATOR_PHOTO_THUMB);
                
                    $post_innovator['photo'] = $uploaded_photo;
                }
            }

            if($this->innovator->update($this->userdata['id'], $post_innovator, true)){
                $status = 1;
                $this->session->set_flashdata('alert','Data has been saved.');
            }else{
                $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');    
            }
        }

        redirect(base_url().'profile');
    }

    private function _upload($name,$attachment,$upload_path, $thumb_path = NULL) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $retval = $id."_".$attachment_name;

        return $retval;
    }

    private function _remove($file_name,$path){
        if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./".$path."/".$file_name);
        }
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect('accounts');
    }
}
