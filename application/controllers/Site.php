<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Site extends Common {

	function __construct() {
		parent::__construct("site");

		$this->layout = DEFAULT_LAYOUT;
		$this->load->model('user_session');

		$this->title = "Site";
		$this->menu = "site";
    }

    public function index(){
        $this->title = "Site";
        $this->scripts[] = "site/site";
        $this->load->view('site/dashboard');
    }
}
