<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	function __construct($module = NULL) {
		parent::__construct();
		$this->layout = DEFAULT_ADMIN_LAYOUT;

		$this->language = LANGUAGE_MELAYU;

		if($module != "account"){
			$this->userdata = $this->user_session->get_admin();
			if (!$this->userdata) {
				redirect(base_url().PATH_TO_ADMIN.'login');
			}	
		}
    }

    function generate_business_plan_pdf($id, $auto_download = TRUE){
        $this->layout = FALSE;
        $this->load->helper(array('dompdf', 'file'));
        $this->load->model('innovation');
        $this->load->model('mara_innovation_business_plan');
        $this->load->model('mara_innovation_business_plan_picture');

        $business_plan = $this->mara_innovation_business_plan->find_one("id = ".$id);
        if($business_plan){
            $data['business_plan'] = $business_plan;
            $data['innovation'] = $this->innovation->find_one("innovation_id = ".$business_plan['innovation_id']);
            $data['business_plan_picture'] = $this->mara_innovation_business_plan_picture->find('mara_innovation_business_plan_id = '.$business_plan['id']);
        }
        
        $filename = "Business_plan_".$data['innovation']['innovation_id']."_".str_replace(" ","_",$data['innovation']['name_in_melayu']).".pdf";
        $html = $this->load->view('innovation/business_plan_form_pdf', $data, true);
        
        generate_pdf($html, $filename,$auto_download); 
    }
}