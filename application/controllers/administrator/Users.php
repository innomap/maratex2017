<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Users extends Common {

	function __construct() {
		parent::__construct("user");

        $this->title = "Users";
        $this->menu = "user";

		$this->load->model('user');

		$this->lang->load(PATH_TO_ADMIN.'user',$this->language);

		$this->scripts[] = 'administrator/user';
    }

    public function index(){
        $data['alert'] = $this->session->flashdata('alert');
        $data['users'] = $this->user->get_many_by('role_id', ROLE_ADMINISTRATOR);
        $this->load->view(PATH_TO_ADMIN.'user/list', $data);
    }

    public function view($user_id){
        $data['detail'] = $this->user->get($user_id);
        $this->load->view(PATH_TO_ADMIN.'user/form', $data);   
    }

    public function save(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        $data = array("email" => $postdata['email'], "password" => $postdata['password']);
    
        if($this->user->update($postdata['id'], $data, true)){
            $this->session->set_flashdata('alert','User password successfully changed');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

        redirect(base_url().PATH_TO_ADMIN.'users');
    }

     private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'users');
    }
}
