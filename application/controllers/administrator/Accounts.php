<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Accounts extends Common {

	function __construct() {
		parent::__construct("account");

		$this->load->model('user');

		$this->lang->load('account',$this->language);

		$this->scripts[] = 'administrator/account';
    }

    public function index(){
		$this->user = $this->user_session->get_admin();
		if ($this->user) {
			redirect(base_url().PATH_TO_ADMIN.'dashboard');
		}else{
			$this->login();
		}
    }

	public function login(){
		$this->title = "Login";
		$data['alert'] = $this->session->flashdata('alert');
		$this->load->view(PATH_TO_ADMIN.'account/form_login',$data);
	}

	function login_auth(){
		$this->layout = FALSE;
		
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		
		$this->auth($email, $password);
	}

	private function auth($email, $password){		
		$user = $this->user->auth($email, $password);
		if($user != NULL){
			if($user->role_id == ROLE_ADMINISTRATOR){
				$data_session = array('id' => $user->id,'email' => $user->email, 'role_id' => $user->role_id);
				$this->user_session->set_admin($data_session);
				redirect(base_url().PATH_TO_ADMIN.'accounts');
			}else{
				$this->session->set_flashdata('alert','The email or password you entered is incorrect.');
				redirect(base_url().PATH_TO_ADMIN.'login');	
			}
		}else{
			$this->session->set_flashdata('alert','The email or password you entered is incorrect.');
			redirect(base_url().PATH_TO_ADMIN.'login');
		}
	}

	public function logout() {
		$this->user_session->clear();
		redirect(base_url().PATH_TO_ADMIN.'login');
	}
}
