<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Innovators extends Common {

	function __construct() {
		parent::__construct("innovator");

        $this->title = "Innovators";
        $this->menu = "innovator";

		$this->load->model(array('innovator', 'user'));

		$this->lang->load(PATH_TO_ADMIN.'innovator',$this->language);

		$this->scripts[] = 'administrator/innovator';
    }

    public function index(){
        $data['alert'] = $this->session->flashdata('alert');
        $data['innovators'] = $this->innovator->get_with_user();
        $this->load->view(PATH_TO_ADMIN.'innovator/list', $data);
    }

    public function view($innovator_id){
        $innovator = $this->innovator->get_with_user(array('i.id' => $innovator_id));
        $data['detail'] = $innovator[0];
        $this->load->view(PATH_TO_ADMIN.'innovator/form', $data);   
    }

    public function save(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        $data = array("email" => $postdata['email'], "password" => $postdata['password']);
    
        if($this->user->update($postdata['id'], $data, true)){
            $this->session->set_flashdata('alert','User password successfully changed');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

        redirect(base_url().PATH_TO_ADMIN.'innovators');
    }

     private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'innovators');
    }
}
