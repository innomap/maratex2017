<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Articles extends Common {

	function __construct() {
		parent::__construct("article");

        $this->title = "Articles";
        $this->menu = "article";

		$this->load->model('article');

		$this->lang->load(PATH_TO_ADMIN.'article',$this->language);

		$this->scripts[] = 'administrator/article';
    }

    public function index(){
        $data['alert'] = $this->session->flashdata('alert');
        $data['articles'] = $this->article->get_all();
        $this->load->view(PATH_TO_ADMIN.'article/list', $data);
    }

    public function add(){
        $data['mode'] = 'add';
        $this->load->view(PATH_TO_ADMIN.'article/form', $data);   
    }

    public function edit($article_id){
        $data['mode'] = 'update';
        $data['detail'] = $this->article->get($article_id);
        $this->load->view(PATH_TO_ADMIN.'article/form', $data);   
    }

    public function save(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        $mode = $postdata['mode'];
    
        $success = false;
        unset($postdata['mode']);
        if ($mode == "add"){
            $insert_id = $this->article->insert($postdata);
            $success = $insert_id > 0;
        }elseif ($mode == "update"){
            $success = $this->article->update($postdata['id'], $postdata);
        }

        if($success){
            $this->session->set_flashdata('alert','Article successfully ' . $mode . 'ed');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

        redirect(base_url().PATH_TO_ADMIN.'articles');
    }

     private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'articles');
    }

    function delete($article_id){
        $this->layout = FALSE;
        
        if($this->article->delete($article_id)){
            $this->session->set_flashdata('alert','Article has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Article can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'articles');
    }
}
