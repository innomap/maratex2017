<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Evaluators extends Common {

	function __construct() {
		parent::__construct("evaluator");

        $this->title = "Evaluators";
        $this->menu = "evaluator";

		$this->load->model(array('user', 'evaluator'));

		$this->lang->load(PATH_TO_ADMIN.'evaluator',$this->language);

		$this->scripts[] = 'administrator/evaluator';
    }

    public function index(){
        $data['alert'] = $this->session->flashdata('alert');
        $data['evaluators'] = $this->evaluator->get_with_user();
        $this->load->view(PATH_TO_ADMIN.'evaluator/list', $data);
    }

    public function add(){
        $data['mode'] = 'add';
        $this->load->view(PATH_TO_ADMIN.'evaluator/form', $data);   
    }

    public function edit($evaluator_id){
        $data['mode'] = 'update';
        $evaluator = $this->evaluator->get_with_user(array('e.id' => $evaluator_id));
        $data['detail'] = $evaluator[0];
        $this->load->view(PATH_TO_ADMIN.'evaluator/form', $data);   
    }

    public function save(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        $mode = $postdata['mode'];
        $user_postdata = array("email"    => $postdata['email'],
                               "password" => $postdata['password'],
                               "role_id"  => ROLE_EVALUATOR,
                               "status"   => $postdata['status']);
        $evaluator_postdata = array("name"    => $postdata['name'],
                                    "address" => $postdata['address'],
                                    "telp_no" => $postdata['telp_no']);
    
        $success = false;
        if ($mode == "add"){
            $user_id = $this->user->insert($user_postdata);
            if($user_id > 0){
                $evaluator_postdata["user_id"] = $user_id;
                $insert_id = $this->evaluator->insert($evaluator_postdata);
                $success = $insert_id > 0;
            }
        }elseif ($mode == "update"){
            if($postdata["password"] == ""){
                unset($user_postdata["password"]);
            }
            $success1 = $this->user->update($postdata['user_id'], $user_postdata);
            if($success1){
                $success = $this->evaluator->update($postdata['user_id'], $evaluator_postdata);
            }
        }

        if($success){
            $this->session->set_flashdata('alert','Evaluator successfully ' . $mode . 'ed');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }

     private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }

    function delete($user_id){
        $this->layout = FALSE;
        
        if($this->user->delete($user_id)){
            $this->session->set_flashdata('alert','Evaluator has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Evaluator can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }
}
