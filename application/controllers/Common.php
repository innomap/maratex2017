<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	function __construct($module = NULL) {
		parent::__construct();

		$this->layout = DEFAULT_LAYOUT;

		$this->language = LANGUAGE_MELAYU;
		$this->submenu = "";

		if($module != "account" && $module != "site"){
			$this->userdata = $this->user_session->get_user();
			if (!$this->userdata) {
				redirect(base_url().'login');
			}	
		}
    }
}