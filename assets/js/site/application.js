var tableApplication= $('#table-application'),
	formApplication = $('#form-application'),
	loadingDialog = $('#please_wait_dialog');

$(function(){
	tableApplication.dataTable();
	initAlert();
	initPopover();
	initImgFancybox();

	initAddPicture();
	initDeletePicture();
	initAddTeamMember();
	initDeleteTeamMember();

	initValidator();
    initApprovalOpt();
    initEdit();
    initView();
    initDelete();
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		});
	}
}

function initPopover(){
	$('.popover-item').popover({
		trigger:'hover',
		container:'body',
		placement: 'bottom'
	});
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initAddPicture(){
	$('.add-picture').on('click', function(){
		var num = $(".picture-item").length;
		str = '<div class="col-md-11">'
		str += 		'<input type="file" class="form-control picture-item" name="picture_'+num+'">';
		str += '</div>';
		$(".picture-wrapper div.item").append(str);
	});
}

function initDeletePicture(){
	$('.delete-picture').on('click', function(){
		var currentId = $(this).data('id'),
			currentName = $(this).data('name');

		formApplication.append('<input type="hidden" name="deleted_picture[]" value="'+currentId+'">');
		formApplication.append('<input type="hidden" name="deleted_picture_name[]" value="'+currentName+'">');

		$(this).parent().remove();
	});
}

function initAddTeamMember(){
	var item_num = formApplication.find('.team-member-id').length;

	formApplication.on('click','.add-team-member', function(){
		if(item_num < 3){
			str =	'<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">';
			str += '<div class="col-md-11 team-member-item">';
			str +=		'<input type="hidden" name="h_team_member_pic_'+item_num+'" value="">';
			str +=		'<input type="hidden" name="i_deleted_team_member_'+item_num+'">';
			str += 		'<div class="col-md-6">';
			str += 			'<input type="text" class="form-control" name="team_member_'+item_num+'" placeholder="Nama Ahli">';
			str +=		'</div>';
			str += 		'<div class="col-md-3">';
			str += 			'<input type="text" class="form-control" name="team_member_kp_'+item_num+'" placeholder="No KP">';
			str +=		'</div>';
			str += 		'<div class="col-md-3">';
			str += 			'<input type="text" class="form-control" name="team_member_telp_'+item_num+'" placeholder="No Telefon">';
			str +=		'</div>';
			str +=		'<div class="col-md-12 margin-tb-10">';
			str +=			'<div class="col-md-1">';
			str +=				'<label>Foto</label>';
			str +=			'</div>';
			str +=			'<div class="col-md-11 no-padding">';
			str +=				'<input type="file"class="form-control" name="team_member_pic_'+item_num+'">';
			str +=			'</div>';
			str +=		'</div>';
			str += '</div>';
			str += '<div class="col-md-1">';
			str +=		'<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>';
			str += '</div>';
			$(".team-member-wrap").append(str);
			item_num++;
		}
	});
}

function initDeleteTeamMember(){
	formApplication.on('click','.delete-team-member', function(){
		$(this).parent().prev('div').remove();
		$(this).parent().remove();
	});
}

function initValidator() {
    var errorMsg1 = "Input tidak sah";

    formApplication.bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        live: 'disabled',
        fields: {
            application_category_id: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            project_title: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            project_description: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            potential: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            significant: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            relevancy: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            outcome: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            readiness_opt: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            readiness: {
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
            sustainability:{
                validators: {
                    notEmpty: {message: errorMsg1},
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        var $form = $(e.target),
            validator = $form.data('bootstrapValidator'),
            submitButton = validator.getSubmitButton();

        if (submitButton.context.name == "btn_send_for_approval") {
            showSubmissionAlert();
        } else {
            //formApplication.bootstrapValidator('defaultSubmit');
            loadingDialog.modal();
            $form.ajaxSubmit({
                dataType: 'json',
                success: function (responseText, statusText, xhr, $form) {
                    loadingDialog.modal('hide');
                    window.location.href = responseText.redirect_url;
                }
            });
        }
    });

    $('#btn-save').on('click', function () {
        formApplication.bootstrapValidator('enableFieldValidators', 'project_title', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'application_category_id', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'project_description', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'potential', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'significant', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'relevancy', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'outcome', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'readiness_opt', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'readiness', false);
        formApplication.bootstrapValidator('enableFieldValidators', 'sustainability', false);         
    });
}

function showSubmissionAlert() {
    bootbox.hideAll();
    bootbox.dialog({
        message: lang_send_for_approval_confirm_message + " ?",
        title: lang_send_application_for_approval,
        onEscape: function () {},
        size: "large",
        buttons: {
            close: {
                label: lang_cancel,
                className: "btn-default flat",
                callback: function () {
                    $(this).modal('hide');
                    formApplication.find('input[type=submit]').prop('disabled', false);
                }
            },
            danger: {
                label: lang_send_for_approval,
                className: "btn-success flat",
                callback: function () {
                    $(this).modal('hide');
                    loadingDialog.modal();

					formApplication.ajaxSubmit({
		                dataType: 'json',
		                data: {btn_send_for_approval: 1},
		                success: function (responseText, statusText, xhr, $form) {
		                    loadingDialog.modal('hide');
		                    window.location.href = responseText.redirect_url;
		                }
		            });
                }
            }
        }
    });
}

function initApprovalOpt(){
    var head_approval = $('input[name=headquarters_approval]').is(':checked');
    toogleApprovalOpt(head_approval,'.head-approval');

    var participant_approval = $('input[name=participant_approval]').is(':checked');
    toogleApprovalOpt(participant_approval,'.participant-approval');

    formApplication.on('click','input[name=headquarters_approval]', function(){
        toogleApprovalOpt($(this).is(':checked'), '.head-approval');
    });

    formApplication.on('click','input[name=participant_approval]', function(){
        toogleApprovalOpt($(this).is(':checked'), '.participant-approval');
    });
}

function toogleApprovalOpt(val, elem){
    if(val){
        if(typeof view_mode != 'undefined'){
            if(view_mode){
                formApplication.find(elem).hide();
            }else{
                formApplication.find(elem).show();
            }
        }
    }else{
        formApplication.find(elem).hide();
    }
}

function initEdit() {
    tableApplication.on('click', '.btn-edit-application', function (e) {
        currentId = $(this).data('id');
        window.location.href = baseUrl + 'applications/edit/' + currentId;
    });
}

function initView() {
    tableApplication.on('click', '.btn-view-application', function (e) {
        currentId = $(this).data('id');
        window.location.href = baseUrl + 'applications/view/' + currentId;
    });
    
    if(typeof view_mode != 'undefined'){
        if(view_mode){
            disabledForm();
        }
    }
}

function disabledForm(){
    $('form').find("input[type=text]").prop('readonly', true);
    $('form').find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
    $('form').find("button, input[type=file], input[type=submit]").hide();
    $('.btn-download-wrap').hide();
    $('form').find('.popover-item').hide();
    $('form').find("button.btn-back").show();
}

function initDelete() {
    tableApplication.on('click', '.btn-delete-application', function (e) {
        currentId = $(this).data('id');
        var name = $(this).data('name');
        bootbox.dialog({
            message: lang_delete_confirm_message + " <strong>" + name + "</strong> ?",
            title: lang_delete_application,
            onEscape: function () {},
            size: "small",
            buttons: {
                close: {
                    label: lang_cancel,
                    className: "btn-default flat",
                    callback: function () {
                        $(this).modal('hide');
                    }
                },
                danger: {
                    label: lang_delete,
                    className: "btn-danger flat",
                    callback: function () {
                        window.location.href = baseUrl + 'applications/delete/' + currentId;
                    }
                }
            }
        });
    });
}