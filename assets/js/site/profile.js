var formProfile = $('#form-profile');

$(function () {
    initAlert();
    initImgFancybox();
    initValidator();
});

function initAlert() {
        if (alert != '') {
                $('.alert-info').removeClass('hide').hide().fadeIn(500, function () {
                        $(this).delay(3000).fadeOut(500);
                })
        }
}

function initInputTypeFile() {
        $(":file").filestyle({
                iconName: "glyphicon glyphicon-inbox",
                buttonText: "Pilih fail",
                placeholder: "Tiada fail dipilih"
        });
        $(":file").attr('accept="image/x-png, image/x-PNG, image/jpeg, image/jpg, image/JPEG, image/JPG"');
}

function initImgFancybox() {
        $('.fancyboxs').fancybox({
                padding: 0,
                openEffect: 'elastic',
                helpers: {
                        overlay: {
                                css: {
                                        'background': 'rgba(50, 50, 50, 0.85)'
                                }
                        }
                }
        });
}

function initValidator() {
        $('.number-only').keypress(function (e) {
                var mInput = $(this).val() + "";
                console.log(mInput);
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                }
        });

        var errorMsg = "Input tidak sah";

        formProfile.bootstrapValidator({
                feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                        name: {
                            validators: {
                                notEmpty: {message: 'Name is required'},
                            }
                        },
                        kp_no: {
                            validators: {
                                notEmpty: {message: 'KP No is required'},
                            }
                        },
                        telp_no: {
                            validators: {
                                notEmpty: {message: 'Telp No is required'},
                            }
                        },
                        fax_no: {
                            validators: {
                                notEmpty: {message: 'Fax No is required'},
                            }
                        },
                        mara_center: {
                            validators: {
                                notEmpty: {message: 'Mara Center is required'},
                            }
                        },
                        mara_address: {
                            validators: {
                                notEmpty: {message: 'Mara Address is required'},
                            }
                        },
                        mara_telp_no: {
                            validators: {
                                notEmpty: {message: 'Mara Telp. No is required'},
                            }
                        },
                        mara_fax_no: {
                            validators: {
                                notEmpty: {message: 'Mara Fax. No is required'},
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {message: 'The Email is required'},
                                emailAddress: {message: 'The value is not a valid email address'},
                                remote: {
                                    message: 'The email already exist',
                                    url: baseUrl+'accounts/check_email_exist/',
                                    data: function(validator) {
                                        return {
                                            email: validator.getFieldElements('email').val(),
                                            id: validator.getFieldElements('user_id').val(),
                                        };
                                    },
                                    type: 'POST'
                                },
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {message: 'The Password is required'},
                            }
                        },
                        retype_password: {
                            validators: {
                                notEmpty: {message: 'The Confirm Password is required'},
                                identical: {
                                    field: 'password',
                                    message: 'The Password and its confirm are not the same'
                                }
                            }
                        }
                }
        });
}
