var tableArticle = $('#table-article'),
	formArticle = $('#form-article');

$(function(){
	tableArticle.dataTable();

	initAlert();
	initValidator();
	initDelete();
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initValidator(){
	formArticle.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			menu_name: {
				validators: {
					notEmpty: {message: 'Menu Name is required'},
				}
			},
			title: {
				validators: {
					notEmpty: {message: 'Title is required'},
				}
			},
			content: {
				validators: {
					notEmpty: {message: 'Content is required'},
				}
			}
		}
	});
}

function initDelete(){
	tableArticle.on('click','.btn-delete-article', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_article,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'articles/delete/'+currentId;
					}
				}
			}
		});
	});
}