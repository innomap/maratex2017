var tableEvaluator = $('#table-evaluator'),
	formEvaluator = $('#form-evaluator');

$(function(){
	tableEvaluator.dataTable();

	initAlert();
	initValidator();
	initDelete();
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initValidator(){
	formEvaluator.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name: {
				validators: {
					notEmpty: {message: 'Name is required'},
				}
			},
			telp_no: {
				validators: {
					notEmpty: {message: 'Telp No is required'},
				}
			},
			address: {
				validators: {
					notEmpty: {message: 'Address is required'},
				}
			},
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
					remote: {
                        message: 'The email already exist',
                        url: baseUrl+'accounts/check_email_exist/',
                        data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val(),
                                id: validator.getFieldElements('user_id').val(),
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'},
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Confirm Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its confirm are not the same'
	                }
				}
			}
		}
	});
}

function initDelete(){
	tableEvaluator.on('click','.btn-delete-evaluator', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_evaluator,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'evaluators/delete/'+currentId;
					}
				}
			}
		});
	});
}