var tableInnovator = $('#table-innovator'),
	formUser = $('#form-user'),
	modalEvaluator = $("#modal-evaluator");

$(function(){
	tableInnovator.dataTable();

	initAlert();
	initValidator();
	initEdit();
	initDelete();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initValidator(){
	formUser.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'},
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Confirm Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its confirm are not the same'
	                }
				}
			}
		}
	});
}

function initEdit(){
	tableInnovator.on('click','.btn-edit-evaluator', function(e){
		formEvaluator.prop('action', adminUrl+'evaluators/update');
		var currentId = $(this).data('id');
		$.get(adminUrl+'evaluators/edit/'+currentId, function(res){
			if(res.status == 1){
				formEvaluator.find('.btn-change-pass').removeClass('hide');
				formEvaluator.find('.collapse-group').addClass('collapse');
				var data = res.data;
				formEvaluator.find('input[name=id]').val(currentId);
				formEvaluator.find('input[name=email]').val(data.email).attr('data-id',currentId);;
				formEvaluator.find('input[name=name]').val(data.name);
				formEvaluator.find('select[name=group_id]').val(data.group.mara_group_expert_id);
				modalEvaluator.modal();
			}
		}, 'json');
	});
	modalEvaluator.on('hidden.bs.modal', function(){
		formEvaluator.attr('action', adminUrl+'evaluators/store')
					.find('input[name=id]').val('');
		formEvaluator.bootstrapValidator('resetForm', true);
		formEvaluator.get(0).reset();
		formEvaluator.find('.btn-change-pass').addClass('hide');
		formEvaluator.find('.collapse-group').removeClass('collapse');
	});

	modalEvaluator.on('click','.btn-change-pass',function(e){
		e.preventDefault();
	    var elem = $(this);
	    var collapse = elem.parent().parent().find('.collapse');
	    collapse.collapse('toggle');
	});
}

function initDelete(){
	tableInnovator.on('click','.btn-delete-evaluator', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_evaluator,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'evaluators/delete/'+currentId;
					}
				}
			}
		});
	});
}